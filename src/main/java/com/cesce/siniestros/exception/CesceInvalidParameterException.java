package com.cesce.siniestros.exception;

public class CesceInvalidParameterException extends CesceException {

	private static final long serialVersionUID = 1L;

	public CesceInvalidParameterException() {
		super("error.InvalidParameter");
	}
	
	public CesceInvalidParameterException(String message) {
		super(message);
	}
	
	public CesceInvalidParameterException(String message, Throwable cause) {
		super(cause, message);
	}
}
