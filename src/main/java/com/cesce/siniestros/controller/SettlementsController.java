package com.cesce.siniestros.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import com.cesce.siniestros.entity.CustomerQueryParams;
import com.cesce.siniestros.entity.NonPaymentSettlement;
import com.cesce.siniestros.entity.Settlement;
import com.cesce.siniestros.entity.SettlementsQueryParams;
import com.cesce.siniestros.service.SettlementsService;

@EnableSwagger2

@RestController
@RequestMapping("/claims/v1")
@Api(value = "settlements", produces = "application/json", tags = "Settlements", description = "Settlements")
public class SettlementsController {

	@Autowired
	SettlementsService settlementsService;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/settlements/{languageCode}/{situationCode}", method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get settlements", notes = "Return a settlement list")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Settlement not found") 
	})
	public @ResponseBody List<Settlement> getSettlements(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable String situationCode,
													CustomerQueryParams custParams,
													SettlementsQueryParams params) {

		List<Settlement> listaDatos = settlementsService.getList(customerCode, languageCode, situationCode, custParams, params);
		return listaDatos;
	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/settlements/{languageCode}/afp/{internalNumber}", method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get settlements by nonpayment", notes = "Return a settlement list")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Settlement not found") 
	})
	public @ResponseBody List<Settlement> getSettlements(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable Double internalNumber,
													@RequestParam(required=false) String customerUserCode,
													@RequestParam(required=false) Double contractNumber) {

		List<Settlement> listaDatos = settlementsService.getList(customerCode, languageCode, internalNumber, customerUserCode, contractNumber);
		return listaDatos;
	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/settlements/{languageCode}/settlement/{settlementNum}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get a settlement", notes = "Return a specified settlement")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Settlement not found") 
	})
	public Settlement getSettlement(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable Integer settlementNum, 
													CustomerQueryParams custParams) {
		
		Settlement settlement = settlementsService.getSettlement(customerCode, languageCode, custParams, settlementNum);
		return settlement;
	}

	

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/settlements/{languageCode}/settlement/nonPayment/{settlementNum}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get a list of nonpayments in a settlement", notes = "Get a list of nonpalyments in a settlement")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Settlement not found") 
	})
	public List<NonPaymentSettlement> getNonPaymentSettlement(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable Integer settlementNum, 
													CustomerQueryParams custParams) {

		List<NonPaymentSettlement> nonpaymentSettlements = settlementsService.getNonPaymentSettlement(customerCode, languageCode, settlementNum, custParams);
		return nonpaymentSettlements;
	}
	
	
	
	
}
