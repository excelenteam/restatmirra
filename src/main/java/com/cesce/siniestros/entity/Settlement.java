package com.cesce.siniestros.entity;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author agustin.rodriguez
 * @version 1.0 18 de Julio de 2017
 *
 * Liquidation data
 */
@ApiModel(value = "Liquidations for a insured entity", description = "")
public class Settlement {
	
	@ApiModelProperty(position = 1, value = "File number", example = "12345678", required = true)
	private BigDecimal fileNum;
	
	@ApiModelProperty(position = 2, value = "Payment limit date", example = "12345678", required = true)
	private BigDecimal paymentLimitDate;
	
	@ApiModelProperty(position = 3, value = "Subscription year", example = "1234", required = true)
	private BigDecimal subscriptionYear;
	
	@ApiModelProperty(position = 4, value = "Liquidation situation code", example = "123", required = true)
	private BigDecimal liqSituationCode;
	
	@ApiModelProperty(position = 5, value = "Liquidation situation description", example = "123ABC..50", required = true)
	private String liqSituationDesc;
	
	@ApiModelProperty(position = 6, value = "Liquidation situation date", example = "12345678", required = true)
	private BigDecimal liqSituationDate;
	
	@ApiModelProperty(position = 7, value = "Approve date of liquidation", example = "12345678", required = false)
	private BigDecimal approveDate;
	
	@ApiModelProperty(position = 8, value = "Real date of payment", example = "12345678", required = false)
	private BigDecimal paymentRealDate;
	
	@ApiModelProperty(position = 9, value = "Accidents committee number", example = "12345678", required = false)
	private BigDecimal committeeNumber;
	
	@ApiModelProperty(position = 10, value = "Have 2 possible values: (N): Normal (C): Complementary", example = "N", required = true)
	private String indLiquidacion;

	public BigDecimal getFileNum() {
		return fileNum;
	}

	public Settlement setFileNum(BigDecimal fileNum) {
		this.fileNum = fileNum;
		return this;
	}

	public BigDecimal getPaymentLimitDate() {
		return paymentLimitDate;
	}

	public Settlement setPaymentLimitDate(BigDecimal paymentLimitDate) {
		this.paymentLimitDate = paymentLimitDate;
		return this;
	}

	public BigDecimal getSubscriptionYear() {
		return subscriptionYear;
	}

	public Settlement setSubscriptionYear(BigDecimal subscriptionYear) {
		this.subscriptionYear = subscriptionYear;
		return this;
	}

	public BigDecimal getLiqSituationCode() {
		return liqSituationCode;
	}

	public Settlement setLiqSituationCode(BigDecimal liqSituationCode) {
		this.liqSituationCode = liqSituationCode;
		return this;
	}

	public String getLiqSituationDesc() {
		return liqSituationDesc;
	}

	public Settlement setLiqSituationDesc(String liqSituationDesc) {
		this.liqSituationDesc = liqSituationDesc;
		return this;
	}

	public BigDecimal getLiqSituationDate() {
		return liqSituationDate;
	}

	public Settlement setLiqSituationDate(BigDecimal liqSituationDate) {
		this.liqSituationDate = liqSituationDate;
		return this;
	}

	public BigDecimal getApproveDate() {
		return approveDate;
	}

	public Settlement setApproveDate(BigDecimal approveDate) {
		this.approveDate = approveDate;
		return this;
	}

	public BigDecimal getPaymentRealDate() {
		return paymentRealDate;
	}

	public Settlement setPaymentRealDate(BigDecimal paymentRealDate) {
		this.paymentRealDate = paymentRealDate;
		return this;
	}

	public BigDecimal getCommitteeNumber() {
		return committeeNumber;
	}

	public Settlement setCommitteeNumber(BigDecimal committeeNumber) {
		this.committeeNumber = committeeNumber;
		return this;
	}

	public String getIndLiquidacion() {
		return indLiquidacion;
	}

	public Settlement setIndLiquidacion(String indLiquidacion) {
		this.indLiquidacion = indLiquidacion;
		return this;
	}
	
}
