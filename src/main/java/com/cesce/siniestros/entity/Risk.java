package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Risk", description = "Risk of non-payment")
public class Risk {
	
	@ApiModelProperty(value = "Amount of the risk limit")
	private BigDecimal amoLimitRisk;
	
	@ApiModelProperty(value = "Description of the risk")
	private String  descRisk;
	
	@ApiModelProperty(value = "Type of the risk")
	private BigDecimal riskType;
	
	
	public BigDecimal getAmoLimitRisk() {
		return amoLimitRisk;
	}
	public Risk setAmoLimitRisk(BigDecimal amoLimitRisk) {
		this.amoLimitRisk = amoLimitRisk;
		return this;
	}
	public String getDescRisk() {
		return descRisk;
	}
	public Risk setDescRisk(String descRisk) {
		this.descRisk = descRisk;
		return this;
	}
	public BigDecimal getRiskType() {
		return riskType;
	}
	public Risk setRiskType(BigDecimal riskType) {
		this.riskType = riskType;
		return this;
	}
	
	
}
