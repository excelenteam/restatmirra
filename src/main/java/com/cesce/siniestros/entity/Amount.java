package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Amount", description = "Amount entity")
public class Amount {
	@ApiModelProperty(value = "Unpaid amount on own account", example = "1.567,65")
	private BigDecimal unpAmountOnOwnAccount;
	
	@ApiModelProperty(value = "Unpaid amount on behalf of the state", example = "1.347,65")
	private BigDecimal unpAmountOnBehalfState;
	
	@ApiModelProperty(value = "Amount covered on own account", example = "1.347,65")
	private BigDecimal amoCoveredOwnAccount;
	
	@ApiModelProperty(value = "Amount covered by state", example = "1.347,65")
	private BigDecimal amoCoveredByState;
	
	@ApiModelProperty(value = "Amount compensable for own account", example = "1.347,65")
	private BigDecimal amoCompenOwnAccount;
	
	@ApiModelProperty(value = "Amount compensable by state", example = "1.347,65")
	private BigDecimal amoCompenByState;
	
	@ApiModelProperty(value = "Amount charged to principal on own account", example = "1.347,65")
	private BigDecimal amoChargedPpalOwnAccount;
	
	@ApiModelProperty(value = "Amount charged to principal by state", example = "1.347,65")
	private BigDecimal amoChargedPpalByState;
	
	@ApiModelProperty(value = "Amount of interest charged on own account", example = "1.347,65")
	private BigDecimal amoInteCharOwnAccount;
	
	@ApiModelProperty(value = "Amount of interest charged by state", example = "1.347,65")
	private BigDecimal amoInteCharByState;
	
	@ApiModelProperty(value = "Principal indemnity amount on own account", example = "1.347,65")
	private BigDecimal amoPpalIndemOwnAccount;
	
	@ApiModelProperty(value = "Principal indemnity amount by state", example = "1.347,65")
	private BigDecimal amoPpalIndemByState;
	
	@ApiModelProperty(value = "Amount indemnified interest on own account", example = "1.347,65")
	private BigDecimal amoIndemInteOwnAccount;
	
	@ApiModelProperty(value = "Amount indemnified interest by state", example = "1.347,65")
	private BigDecimal amoIndemInteByState;
	
	@ApiModelProperty(value = "Amount principal recovered on own account", example = "1.347,65")
	private BigDecimal amoPpalRecovOwnAccount;
	
	@ApiModelProperty(value = "Amount principal recovered by state", example = "1.347,65")
	private BigDecimal amoPpalRecovByState;
	
	@ApiModelProperty(value = "Amount recovered for own account", example = "1.347,65")
	private BigDecimal amoRecovOwnAccount;
	
	@ApiModelProperty(value = "Amount recovered by state", example = "1.347,65")
	private BigDecimal amoRecovByState;
	
	@ApiModelProperty(value = "Amount pending compensation for own account", example = "1.347,65")
	private BigDecimal amoPendCompOwnAccount;
	
	@ApiModelProperty(value = "Amount pending compensation by state", example = "1.347,65")
	private BigDecimal amoPendCompByState;
	
	@ApiModelProperty(value = "Uncollectible amount", example = "1.347,65")
	private BigDecimal amoUncollectible;
	
	@ApiModelProperty(value = "Recognized credit amount", example = "1.347,65")
	private BigDecimal amoRecogCredit;
	
	@ApiModelProperty(value = "Redemption amount for CESCE", example = "1.347,65")
	private BigDecimal amoRedemForCESCE;
	
	@ApiModelProperty(value = "Amount of removal by the insured", example = "1.347,65")
	private BigDecimal amoRemovByInsured;
	
	
	@ApiModelProperty(value = "Amount of judgment in commercial dispute", example = "1.347,65")
	private BigDecimal amoJudgCommerDispute;
	
	@ApiModelProperty(value = "Amount of franchise consumed", example = "1.347,65")
	private BigDecimal amoFranchConsumed;
	
	@ApiModelProperty(value = "Amount of franchise not consumed", example = "1.347,65")
	private BigDecimal amoFranchNotConsumed;

	public BigDecimal getUnpAmountOnOwnAccount() {
		return unpAmountOnOwnAccount;
	}

	public Amount setUnpAmountOnOwnAccount(BigDecimal unpAmountOnOwnAccount) {
		this.unpAmountOnOwnAccount = unpAmountOnOwnAccount;
		return this;
	}

	public BigDecimal getUnpAmountOnBehalfState() {
		return unpAmountOnBehalfState;
	}

	public Amount setUnpAmountOnBehalfState(BigDecimal unpAmountOnBehalfState) {
		this.unpAmountOnBehalfState = unpAmountOnBehalfState;
		return this;
	}

	public BigDecimal getAmoCoveredOwnAccount() {
		return amoCoveredOwnAccount;
	}

	public Amount setAmoCoveredOwnAccount(BigDecimal amoCoveredOwnAccount) {
		this.amoCoveredOwnAccount = amoCoveredOwnAccount;
		return this;
	}

	public BigDecimal getAmoCoveredByState() {
		return amoCoveredByState;
	}

	public Amount setAmoCoveredByState(BigDecimal amoCoveredByState) {
		this.amoCoveredByState = amoCoveredByState;
		return this;
	}

	public BigDecimal getAmoCompenOwnAccount() {
		return amoCompenOwnAccount;
	}

	public Amount setAmoCompenOwnAccount(BigDecimal amoCompenOwnAccount) {
		this.amoCompenOwnAccount = amoCompenOwnAccount;
		return this;
	}

	public BigDecimal getAmoCompenByState() {
		return amoCompenByState;
	}

	public Amount setAmoCompenByState(BigDecimal amoCompenByState) {
		this.amoCompenByState = amoCompenByState;
		return this;
	}

	public BigDecimal getAmoChargedPpalOwnAccount() {
		return amoChargedPpalOwnAccount;
	}

	public Amount setAmoChargedPpalOwnAccount(BigDecimal amoChargedPpalOwnAccount) {
		this.amoChargedPpalOwnAccount = amoChargedPpalOwnAccount;
		return this;
	}

	public BigDecimal getAmoChargedPpalByState() {
		return amoChargedPpalByState;
	}

	public Amount setAmoChargedPpalByState(BigDecimal amoChargedPpalByState) {
		this.amoChargedPpalByState = amoChargedPpalByState;
		return this;
	}

	public BigDecimal getAmoInteCharOwnAccount() {
		return amoInteCharOwnAccount;
	}

	public Amount setAmoInteCharOwnAccount(BigDecimal amoInteCharOwnAccount) {
		this.amoInteCharOwnAccount = amoInteCharOwnAccount;
		return this;
	}

	public BigDecimal getAmoInteCharByState() {
		return amoInteCharByState;
	}

	public Amount setAmoInteCharByState(BigDecimal amoInteCharByState) {
		this.amoInteCharByState = amoInteCharByState;
		return this;
	}

	public BigDecimal getAmoPpalIndemOwnAccount() {
		return amoPpalIndemOwnAccount;
	}

	public Amount setAmoPpalIndemOwnAccount(BigDecimal amoPpalIndemOwnAccount) {
		this.amoPpalIndemOwnAccount = amoPpalIndemOwnAccount;
		return this;
	}

	public BigDecimal getAmoPpalIndemByState() {
		return amoPpalIndemByState;
	}

	public Amount setAmoPpalIndemByState(BigDecimal amoPpalIndemByState) {
		this.amoPpalIndemByState = amoPpalIndemByState;
		return this;
	}

	public BigDecimal getAmoIndemInteOwnAccount() {
		return amoIndemInteOwnAccount;
	}

	public Amount setAmoIndemInteOwnAccount(BigDecimal amoIndemInteOwnAccount) {
		this.amoIndemInteOwnAccount = amoIndemInteOwnAccount;
		return this;
	}

	public BigDecimal getAmoIndemInteByState() {
		return amoIndemInteByState;
	}

	public Amount setAmoIndemInteByState(BigDecimal amoIndemInteByState) {
		this.amoIndemInteByState = amoIndemInteByState;
		return this;
	}

	public BigDecimal getAmoPpalRecovOwnAccount() {
		return amoPpalRecovOwnAccount;
	}

	public Amount setAmoPpalRecovOwnAccount(BigDecimal amoPpalRecovOwnAccount) {
		this.amoPpalRecovOwnAccount = amoPpalRecovOwnAccount;
		return this;
	}

	public BigDecimal getAmoPpalRecovByState() {
		return amoPpalRecovByState;
	}

	public Amount setAmoPpalRecovByState(BigDecimal amoPpalRecovByState) {
		this.amoPpalRecovByState = amoPpalRecovByState;
		return this;
	}

	public BigDecimal getAmoRecovOwnAccount() {
		return amoRecovOwnAccount;
	}

	public Amount setAmoRecovOwnAccount(BigDecimal amoRecovOwnAccount) {
		this.amoRecovOwnAccount = amoRecovOwnAccount;
		return this;
	}

	public BigDecimal getAmoRecovByState() {
		return amoRecovByState;
	}

	public Amount setAmoRecovByState(BigDecimal amoRecovByState) {
		this.amoRecovByState = amoRecovByState;
		return this;
	}

	public BigDecimal getAmoPendCompOwnAccount() {
		return amoPendCompOwnAccount;
	}

	public Amount setAmoPendCompOwnAccount(BigDecimal amoPendCompOwnAccount) {
		this.amoPendCompOwnAccount = amoPendCompOwnAccount;
		return this;
	}

	public BigDecimal getAmoPendCompByState() {
		return amoPendCompByState;
	}

	public Amount setAmoPendCompByState(BigDecimal amoPendCompByState) {
		this.amoPendCompByState = amoPendCompByState;
		return this;
	}

	public BigDecimal getAmoUncollectible() {
		return amoUncollectible;
	}

	public Amount setAmoUncollectible(BigDecimal amoUncollectible) {
		this.amoUncollectible = amoUncollectible;
		return this;
	}

	public BigDecimal getAmoRecogCredit() {
		return amoRecogCredit;
	}

	public Amount setAmoRecogCredit(BigDecimal amoRecogCredit) {
		this.amoRecogCredit = amoRecogCredit;
		return this;
	}

	public BigDecimal getAmoRedemForCESCE() {
		return amoRedemForCESCE;
	}

	public Amount setAmoRedemForCESCE(BigDecimal amoRedemForCESCE) {
		this.amoRedemForCESCE = amoRedemForCESCE;
		return this;
	}

	public BigDecimal getAmoRemovByInsured() {
		return amoRemovByInsured;
	}

	public Amount setAmoRemovByInsured(BigDecimal amoRemovByInsured) {
		this.amoRemovByInsured = amoRemovByInsured;
		return this;
	}

	public BigDecimal getAmoJudgCommerDispute() {
		return amoJudgCommerDispute;
	}

	public Amount setAmoJudgCommerDispute(BigDecimal amoJudgCommerDispute) {
		this.amoJudgCommerDispute = amoJudgCommerDispute;
		return this;
	}

	public BigDecimal getAmoFranchConsumed() {
		return amoFranchConsumed;
	}

	public Amount setAmoFranchConsumed(BigDecimal amoFranchConsumed) {
		this.amoFranchConsumed = amoFranchConsumed;
		return this;
	}

	public BigDecimal getAmoFranchNotConsumed() {
		return amoFranchNotConsumed;
	}

	public Amount setAmoFranchNotConsumed(BigDecimal amoFranchNotConsumed) {
		this.amoFranchNotConsumed = amoFranchNotConsumed;
		return this;
	}
	
	
}
