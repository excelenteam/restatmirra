package com.cesce.siniestros.entity;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author agustin.rodriguez
 * @version 1.0 18 de Julio de 2017
 *
 * Liquidation data
 */
@ApiModel(value = "Liquidations for a insured entity", description = "")
public class Recovers {

	@ApiModelProperty(position = 1, value = "Insured code", example = "123456789", required = true)
	private BigDecimal insuredCode;
	
	@ApiModelProperty(position = 2, value = "Insured name", example = "123ABC..100", required = true)
	private String insuredName;

	@ApiModelProperty(position = 3, value = "Recoveries list")
	private List<RecoveryItem> recoveriesList;

	public BigDecimal getInsuredCode() {
		return insuredCode;
	}

	public Recovers setInsuredCode(BigDecimal insuredCode) {
		this.insuredCode = insuredCode;
		return this;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public Recovers setInsuredName(String insuredName) {
		this.insuredName = insuredName;
		return this;
	}

	public List<RecoveryItem> getRecoveriesList() {
		return recoveriesList;
	}

	public Recovers setRecoveriesList(List<RecoveryItem> recoveriesList) {
		this.recoveriesList = recoveriesList;
		return this;
	}
}
