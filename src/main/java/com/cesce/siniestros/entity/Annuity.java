package com.cesce.siniestros.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Annuity", description = "Entity of Annuity")
public class Annuity {
	
	@ApiModelProperty(value = "Annuity currency code (ISO)")
	private String annCurrCode;
	
	@ApiModelProperty(value = "Description of annuity currency")
	private String annCurrDesc;
	
	
	@ApiModelProperty(value = "Annuity currency maturity amount")
	private Double annCurrMatuAmount;
	

	@ApiModelProperty(value = "Amount of default in annuity currency")
	private Double amoNonPayAnnCurrency;
	
	@ApiModelProperty(value = "Amount live in annuity currency")
	private Double amoLiveAnnCurrency;
	
	@ApiModelProperty(value = "Amount covered in annuity currency")
	private Double amoCoverAnnCurrency;
	
	@ApiModelProperty(value = "Amount compensable in annuity currency")
	private Double amoCompAnnCurrency;
	
	@ApiModelProperty(value = "Amount compensable for per cent coverage in annuity currency")
	private Double amoCompPercentCoverAnnCurrency;
	 
	@ApiModelProperty(value = "AmountNOT compensable in annuity currency")
	private Double amoNOTcompAnnCurrecy;
	
	@ApiModelProperty(value = "Amount NOT covered in annuity currency")
	private Double amoNOTcoverAnnCurrecy;
	
	@ApiModelProperty(value = "Amount paid in annuity currency")
	private Double amoPaidAnnCurrecy;
	
	@ApiModelProperty(value = "Amount charged in annuity currency")
	private Double amoChargAnnCurrecy;
	
	@ApiModelProperty(value = "Amount recovered in annuity currency")
	private Double amoRecovAnnCurrecy;

	public String getAnnCurrCode() {
		return annCurrCode;
	}

	public void setAnnCurrCode(String annCurrCode) {
		this.annCurrCode = annCurrCode;
	}

	public String getAnnCurrDesc() {
		return annCurrDesc;
	}

	public Annuity setAnnCurrDesc(String annCurrDesc) {
		this.annCurrDesc = annCurrDesc;
		return this;
	}

	public Double getAnnCurrMatuAmount() {
		return annCurrMatuAmount;
	}

	public Annuity setAnnCurrMatuAmount(Double annCurrMatuAmount) {
		this.annCurrMatuAmount = annCurrMatuAmount;
		return this;
	}

	public Double getAmoNonPayAnnCurrency() {
		return amoNonPayAnnCurrency;
	}

	public Annuity setAmoNonPayAnnCurrency(Double amoNonPayAnnCurrency) {
		this.amoNonPayAnnCurrency = amoNonPayAnnCurrency;
		return this;
	}

	public Double getAmoLiveAnnCurrency() {
		return amoLiveAnnCurrency;
	}

	public void setAmoLiveAnnCurrency(Double amoLiveAnnCurrency) {
		this.amoLiveAnnCurrency = amoLiveAnnCurrency;
	}

	public Double getAmoCoverAnnCurrency() {
		return amoCoverAnnCurrency;
	}

	public Annuity setAmoCoverAnnCurrency(Double amoCoverAnnCurrency) {
		this.amoCoverAnnCurrency = amoCoverAnnCurrency;
		return this;
	}

	public Double getAmoCompAnnCurrency() {
		return amoCompAnnCurrency;
	}

	public Annuity setAmoCompAnnCurrency(Double amoCompAnnCurrency) {
		this.amoCompAnnCurrency = amoCompAnnCurrency;
		return this;
	}

	public Double getAmoCompPercentCoverAnnCurrency() {
		return amoCompPercentCoverAnnCurrency;
	}

	public Annuity setAmoCompPercentCoverAnnCurrency(Double amoCompPercentCoverAnnCurrency) {
		this.amoCompPercentCoverAnnCurrency = amoCompPercentCoverAnnCurrency;
		return this;
	}

	public Double getAmoNOTcompAnnCurrecy() {
		return amoNOTcompAnnCurrecy;
	}

	public Annuity setAmoNOTcompAnnCurrecy(Double amoNOTcompAnnCurrecy) {
		this.amoNOTcompAnnCurrecy = amoNOTcompAnnCurrecy;
		return this;
	}

	public Double getAmoNOTcoverAnnCurrecy() {
		return amoNOTcoverAnnCurrecy;
	}

	public Annuity setAmoNOTcoverAnnCurrecy(Double amoNOTcoverAnnCurrecy) {
		this.amoNOTcoverAnnCurrecy = amoNOTcoverAnnCurrecy;
		return this;
	}

	public Double getAmoPaidAnnCurrecy() {
		return amoPaidAnnCurrecy;
	}

	public Annuity setAmoPaidAnnCurrecy(Double amoPaidAnnCurrecy) {
		this.amoPaidAnnCurrecy = amoPaidAnnCurrecy;
		return this;
	}

	public Double getAmoChargAnnCurrecy() {
		return amoChargAnnCurrecy;
	}

	public Annuity setAmoChargAnnCurrecy(Double amoChargAnnCurrecy) {
		this.amoChargAnnCurrecy = amoChargAnnCurrecy;
		return this;
	}

	public Double getAmoRecovAnnCurrecy() {
		return amoRecovAnnCurrecy;
	}

	public Annuity setAmoRecovAnnCurrecy(Double amoRecovAnnCurrecy) {
		this.amoRecovAnnCurrecy = amoRecovAnnCurrecy;
		return this;
	}
}
