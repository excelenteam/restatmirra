package com.cesce.siniestros.entity;

import io.swagger.annotations.ApiParam;

public class SettlementsQueryParams {
	
	@ApiParam(value = "Number of file", example = "12345678", required=false)
	public Integer fileNum;

	@ApiParam(value = "Number of supplement", example = "123456", required=false)
	public Integer supplementNum;

	@ApiParam(value = "Name of the debtor", example = "1234ABCD..100", required=false)
	public String debtorName;

	@ApiParam(value = "ISO code of debtor's country", example = "12A", required=false)
	public String debtorCountryCode;

	@ApiParam(value = "Fiscal identifier code of the debtor", example = "1234ABCD..20", required=false)
	public String debtorFiscalCode;

	@ApiParam(value = "Debtor code in the customer", example = "1234ABCD..50", required=false)
	public String referenceCode;

	public Integer getFileNum() {
		return fileNum;
	}

	public SettlementsQueryParams setFileNum(Integer fileNum) {
		this.fileNum = fileNum;
		return this;
	}

	public Integer getSupplementNum() {
		return supplementNum;
	}

	public SettlementsQueryParams setSupplementNum(Integer supplementNum) {
		this.supplementNum = supplementNum;
		return this;
	}

	public String getDebtorName() {
		return debtorName;
	}

	public SettlementsQueryParams setDebtorName(String debtorName) {
		this.debtorName = debtorName;
		return this;
	}

	public String getDebtorCountryCode() {
		return debtorCountryCode;
	}

	public SettlementsQueryParams setDebtorCountryCode(String debtorCountryCode) {
		this.debtorCountryCode = debtorCountryCode;
		return this;
	}

	public String getDebtorFiscalCode() {
		return debtorFiscalCode;
	}

	public SettlementsQueryParams setDebtorFiscalCode(String debtorFiscalCode) {
		this.debtorFiscalCode = debtorFiscalCode;
		return this;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public SettlementsQueryParams setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
		return this;
	}	
}