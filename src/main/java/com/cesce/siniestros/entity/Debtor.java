package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value = "Debtor", description = "Debtor entity")
public class Debtor {

		@ApiModelProperty(value = "Debtor's code", example = "677890200")
		private BigDecimal debtorCode;
		
		@ApiModelProperty(value = "Debtor's name", example = "Johan Miles")
		private String debtorName;
		
		@ApiModelProperty(value = "Debtor's country", example = "038")
		private BigDecimal debtorCountry;
		
		@ApiModelProperty(value = "Description of the debtor country", example = "European country") 
		private String debtorCountrydescription;

		public BigDecimal getDebtorCode() {
			return debtorCode;
		}

		public Debtor setDebtorCode(BigDecimal debtorCode) {
			this.debtorCode = debtorCode;
			return this;
		}

		public String getDebtorName() {
			return debtorName;
		}

		public Debtor setDebtorName(String debtorName) {
			this.debtorName = debtorName;
			return this;
		}

		

		public BigDecimal getDebtorCountry() {
			return debtorCountry;
		}

		public Debtor setDebtorCountry(BigDecimal debtorCountry) {
			this.debtorCountry = debtorCountry;
			return this;
		}

		public String getDebtorCountrydescription() {
			return debtorCountrydescription;
		}

		public Debtor setDebtorCountrydescription(String debtorCountrydescription) {
			this.debtorCountrydescription = debtorCountrydescription;
			return this;
		}

		 

		 

		 
		
		
}
