package com.cesce.siniestros.service;

import java.util.List;

import com.cesce.siniestros.entity.NonPayment;
import com.cesce.siniestros.entity.NonPayments;

public interface NonPaymentService {
	 List<NonPayments> findAll();
	 
	 NonPayment findOne(Integer id);
}
