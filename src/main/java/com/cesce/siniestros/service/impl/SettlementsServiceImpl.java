package com.cesce.siniestros.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cesce.siniestros.entity.CustomerQueryParams;
import com.cesce.siniestros.entity.NonPaymentSettlement;
import com.cesce.siniestros.entity.Settlement;
import com.cesce.siniestros.entity.SettlementsQueryParams;
import com.cesce.siniestros.repository.SettlementsRepository;
import com.cesce.siniestros.service.SettlementsService;

/**
 * 
 * @author agustin.rodriguez
 *
 */
@Service("settlementsService")
public class SettlementsServiceImpl implements SettlementsService{

	@Autowired
	SettlementsRepository settlementsRepository;

	@Override
	public List<Settlement> getList(Integer customerCode, String situationCode, String languageCode,
			CustomerQueryParams custParams, SettlementsQueryParams params) {
		return settlementsRepository.getList(customerCode, languageCode, situationCode, custParams, params);
	}

	@Override
	public List<Settlement> getList(Integer customerCode, String languageCode, Double internalNumber,
			String customerUserCode, Double contractNumber) {
		return settlementsRepository.getList(customerCode, languageCode, internalNumber, customerUserCode, contractNumber);
	}

	@Override
	public Settlement getSettlement(Integer customerCode, String languageCode, CustomerQueryParams custParams,
			Integer settlementNum) {
		return settlementsRepository.getSettlement(customerCode, languageCode, custParams, settlementNum);
	}

	@Override
	public List<NonPaymentSettlement> getNonPaymentSettlement(Integer customerCode, String languageCode,
			Integer settlementNum, CustomerQueryParams custParams) {
		return settlementsRepository.getNonPaymentSettlement(customerCode, languageCode, settlementNum, custParams);
	}
	

	
	
	
}
