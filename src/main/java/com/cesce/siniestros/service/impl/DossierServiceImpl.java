package com.cesce.siniestros.service.impl;

import com.cesce.siniestros.entity.CustomerQueryParams;
import com.cesce.siniestros.entity.Dossier;
import com.cesce.siniestros.entity.DossierDocs;
import com.cesce.siniestros.entity.DossierProcess;
import com.cesce.siniestros.entity.NonPaymentSettlement;
import com.cesce.siniestros.entity.Recovers;
import com.cesce.siniestros.entity.SettlementAwaredDate;
import com.cesce.siniestros.repository.DossierRepository;
import com.cesce.siniestros.service.DossierService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DossierServiceImpl implements DossierService {

    @Autowired
    private DossierRepository dossierRepository;

	@Override
	public Dossier getDossierByID(Integer customerCode, String languageCode, Integer dossierNumber) {
		return dossierRepository.getDossierByID(customerCode, languageCode, dossierNumber);
	}

	@Override
	public Dossier getClassificationSupplements(Integer customerCode, String languageCode, Integer contractNumber,
			Integer suppNumber, Integer debtorCode, String originId, String customerUserCode, Integer dossierNumber) {
		return dossierRepository.getClassificationSupplements(customerCode, languageCode, contractNumber, 
				suppNumber, debtorCode, originId, customerUserCode, dossierNumber);
	}

	@Override
	public List<Dossier> getListDossiers(Integer customerCode, String languageCode, String situationCode,
			CustomerQueryParams custParams, Integer dossierNum, Integer supplementNum, String debtorName,
			String debtorCountryCode, String debtorFiscalCode, String referenceCode){
		return dossierRepository.getListDossiers(customerCode, languageCode, situationCode, custParams, dossierNum,
				supplementNum, debtorName, debtorCountryCode, debtorFiscalCode, referenceCode);
	} 
	@Override
	public DossierDocs getDossierDocumentList(Integer customerCode, String languageCode, Integer ocurrenceNum,
			CustomerQueryParams custParams, Integer dossierNum, Integer supplementNum, Integer inputReplacementValue) {
		return dossierRepository.getDossierDocumentList(customerCode, languageCode, ocurrenceNum, custParams, dossierNum, supplementNum, inputReplacementValue);
	}
	
	@Override
	public List<SettlementAwaredDate> getPendingDates(Integer customerCode, String languageCode, Integer dossierNum,
			CustomerQueryParams custParams) {
		return dossierRepository.getPendingDates(customerCode, languageCode, dossierNum, custParams);
	}
	

	@Override
	public List<Recovers> getListRecovers(Integer customerCode, String languageCode, Integer dossierNum,
			CustomerQueryParams custParams){
		return dossierRepository.getListRecovers(customerCode, languageCode, dossierNum, custParams);
	}
	
	
	@Override
	public List<String> getDossiersManagerRecovery(Integer customerCode, String languageCode, String dossierNum,
			CustomerQueryParams custParams) {
		return new ArrayList<String>();
	}
	

	

	@Override
	public List<DossierProcess> getDossierManagerProcessing(Integer customerCode, String languageCode, String dossierNum,
			CustomerQueryParams custParams){
		return dossierRepository.getDossierManagerProcessing(customerCode, languageCode, dossierNum, custParams);
	}

	 

}
