package com.cesce.siniestros.service.impl;

import com.cesce.siniestros.entity.NonPayment;
import com.cesce.siniestros.entity.NonPayments;
import com.cesce.siniestros.repository.NonPaymentRepository;
import com.cesce.siniestros.service.NonPaymentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NonPaymentServiceImpl implements NonPaymentService {

    @Autowired
    private NonPaymentRepository nonPaymentRepository;
 
    @Override
    public List<NonPayments> findAll() {
        return nonPaymentRepository.findAll();
    }

	@Override
	public NonPayment findOne(Integer id) {
		// TODO Auto-generated method stub
		return nonPaymentRepository.findOne(id);
	}
 
}
