package com.cesce.siniestros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsRestSiniestrosApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsRestSiniestrosApplication.class, args);
	}
}
