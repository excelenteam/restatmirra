package com.cesce.siniestros.repository.impl;

 
import com.cesce.siniestros.entity.NonPayment;
import com.cesce.siniestros.entity.NonPayments;
import com.cesce.siniestros.repository.NonPaymentRepository;


import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class NonPaymentRepositoryMock implements NonPaymentRepository {

	 

	@Override
	public List<NonPayments> findAll() {
		return Arrays.asList(
				new NonPayments().setInvNumber("XCD12322"),
				 
				new NonPayments().setInvNumber("XCD12323"));
	}

	@Override
	public NonPayment findOne(Integer id) {
		// TODO Auto-generated method stub
		return new NonPayment()
				.setClassText("lorem ipsum");
	}

	
}
