package com.pricespy.service.impl;

import java.time.Duration;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProductPrices;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.engine.PriceSearchEngine;
import com.pricespy.engine.provider.CarrefourSearchEngine;
import com.pricespy.engine.provider.CorteInglesSearchEngine;
import com.pricespy.engine.provider.KyerooSearchEngine;
import com.pricespy.engine.provider.MediaMarktSearchEngine;
import com.pricespy.engine.provider.MiElectroSearchEngine;
import com.pricespy.engine.provider.PCComponentesSearchEngine;
import com.pricespy.repository.PriceRepository;
import com.pricespy.service.PriceService;
import com.pricespy.utils.DateUtils;

@Service
public class PriceServiceImpl implements PriceService {

	private static final Logger logger = LoggerFactory.getLogger(PriceServiceImpl.class);

	@Autowired
	private PriceRepository priceRepository;

	@Autowired
	private PriceSearchEngine engine;

	@Override
	public ProductPrices getPricesByProductTest(Product product) {
		ProductPrices prices = new ProductPrices();

		ProviderPrice prov1 = new ProviderPrice();
		prov1.withProviderID(1).withProviderName("MediaMarkt").withProviderPrice(100.54D).setSucessPerc(95);
		prices.addProviderPrice(prov1);

		prov1 = new ProviderPrice();
		prov1.withProviderID(1).withProviderName("Carrefour").withProviderPrice(95.32D).setSucessPerc(80);
		prices.addProviderPrice(prov1);

		prov1 = new ProviderPrice();
		prov1.withProviderID(1).withProviderName("Corte ingles").withProviderPrice(89.94D).setSucessPerc(79);
		prices.addProviderPrice(prov1);

		prov1 = new ProviderPrice();
		prov1.withProviderID(1).withProviderName("PcComponentes").withProviderPrice(89.94D).setSucessPerc(79);
		prices.addProviderPrice(prov1);

		return prices;
	}

	@Override
	public ProductPrices getPricesByProduct(Product product) throws Exception {
		ProductPrices prices = new ProductPrices();
		ProviderPrice provPrice;
		Instant startAllProcess = Instant.now();
		String processTime;

		provPrice = engine.getPrice(new CarrefourSearchEngine(), product);
		prices.addProviderPrice(provPrice);

		provPrice = engine.getPrice(new CorteInglesSearchEngine(), product);
		prices.addProviderPrice(provPrice);

		provPrice = engine.getPrice(new KyerooSearchEngine(), product);
		prices.addProviderPrice(provPrice);

		provPrice = engine.getPrice(new MediaMarktSearchEngine(), product);
		prices.addProviderPrice(provPrice);

		provPrice = engine.getPrice(new MiElectroSearchEngine(), product);
		prices.addProviderPrice(provPrice);

		provPrice = engine.getPrice(new PCComponentesSearchEngine(), product);
		prices.addProviderPrice(provPrice);

		// provPrice = engine.getPrice(new AmazonSearchEngine(), product);
		// prices.addProviderPrice(provPrice);

		// End all providers
		processTime = DateUtils.getDurationFormatting(Duration.between(startAllProcess, Instant.now()));
		logger.info("AllProcess:  " + processTime);
		prices.setProcessTime(processTime);
		return prices;
	}

}
