package com.pricespy.service;

import java.util.List;

import com.pricespy.bean.CustomerQueryParams;
import com.pricespy.bean.Product;
import com.pricespy.bean.ProductPrices;

public interface PriceService {

	ProductPrices getPricesByProduct(Product product) throws Exception;
	
	ProductPrices getPricesByProductTest(Product product);
}
