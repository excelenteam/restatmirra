package com.pricespy.engine.provider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.engine.web.HttpSupport;

public class AmazonSearchEngine implements SearchEngine {

	private static final String PROVIDER_NAME = "Amazon";
	private static final String PROVIDER_DOMAIN = "https://www.amazon.es/s/";
	// https://www.amazon.es/s/ref=nb_sb_noss?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&url=search-alias%3Dcomputers&field-keywords=8806086523035

	@Override
	public ProviderPrice getPrice(Product product) throws Exception {
		// https://www.carrefour.es/global/?Ntt=8806088961361

		ProviderPrice price = new ProviderPrice();

		String url = PROVIDER_DOMAIN + "field-keywords=" + product.getEan();
		String webPage = HttpSupport.getInstance().getWebSite(url);

		String newUrl, tryWebPage = "";
		Document doc = Jsoup.parse(webPage);

		Elements resultList = doc.getElementById("result_0").getElementsByClass("a-fixed-left-grid");
		newUrl = doc.getElementById("result_0").getElementsByClass("s-access-detail-page").get(0).attr("href");

		// tryWebPage = HttpSupport.getInstance().getProtectWebSite(newUrl);
		tryWebPage = HttpSupport.getInstance().getWebSite(newUrl);

		if (tryWebPage.contains(product.getModel()) && tryWebPage.contains(product.getEan())) {

			doc = Jsoup.parse(tryWebPage);
			String priceTemp = doc.getElementById("priceblock_ourprice").text();

			price.setPrice(priceTemp.substring(4).replaceAll("(\\d+),(\\d+)", "$1\\.$2"));
			price.setSucessPerc(98);
			return price;
		}

		return price;
	}

	@Override
	public String getProviderName() {
		return PROVIDER_NAME;
	}

}
