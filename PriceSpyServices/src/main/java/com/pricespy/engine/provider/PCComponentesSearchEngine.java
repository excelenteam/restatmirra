package com.pricespy.engine.provider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.engine.web.HttpSupport;

public class PCComponentesSearchEngine implements SearchEngine {

	private static final String PROVIDER_NAME = "PcComponentes";
	private static final String PROVIDER_DOMAIN = "www.pccomponentes.com";
	// https://www.bing.com/search?q="MZ-75E500B%2FEU"+site:www.pccomponentes.com

	@Override
	public ProviderPrice getPrice(Product product) throws Exception {

		ProviderPrice price = new ProviderPrice();

		// https://www.pccomponentes.com/buscar/?query=9H.LFTLA.TPE
		// Encoding de la url
		String url = "https://www.bing.com/search?q=\"" + product.getModel() + "\"+site:" + PROVIDER_DOMAIN;
		// String url =
		// "https://www.pccomponentes.com/samsung-850-evo-ssd-series-500gb--sata3";
		String webPage = HttpSupport.getInstance().getWebSite(url);

		if (webPage.contains("id=\"b_tween")) {
			//
			Document doc = Jsoup.parse(webPage);

			Element resultNum = (doc.getElementById("b_tween").getElementsByClass("sb_count")).first();
			String textResults = resultNum.childNode(0).toString();
			System.out.println("Return: " + textResults);
			System.out.println("Resultados: " + textResults.substring(0, textResults.length() - 11));

			Elements links = doc.getElementsByClass("b_algo");

			// No se si habrá más de un link de este tipo. igual deberia buscar todos los
			// cites dentro de esta etiqueta
			String newUrl, tryWebPage = "";
			boolean found = false;
			for (Element iter : links) {
				Elements results = iter.getElementsByTag("cite");

				for (Element citeTag : results) {
					newUrl = "";
					for (Node partUrl : citeTag.childNodes()) {
						if (partUrl instanceof Element) {
							newUrl += partUrl.childNode(0).toString();
						} else {
							newUrl += partUrl.toString();
						}
					}
					System.out.println("URL of the seacher: " + newUrl);

					// tryWebPage = HttpSupport.getInstance().getProtectWebSite(newUrl);
					newUrl = "http" + newUrl.substring(4);
					tryWebPage = HttpSupport.getInstance().getWebSite(newUrl);

					if (tryWebPage.contains(product.getModel())) {
						found = true;
						doc = Jsoup.parse(tryWebPage);

						price.setPrice(doc.getElementById("priceBlock").attr("data-price"));
						price.setSucessPerc(75);
						return price;
					}
				}
			}
		}

		// System.out.println("Page: " + tryWebPage);

		return null;
	}

	// @Override
	// public ProviderPrice getPrice(Product product) {
	//
	// ProviderPrice price = new ProviderPrice();
	// try {
	// // https://www.pccomponentes.com/buscar/?query=9H.LFTLA.TPE
	// String url = PROVIDER_DOMAIN + "/buscar/?query=" + product.getModel();
	// String webPage = HttpSupport.getInstance().getWebSite(url);
	//
	// Document doc = Jsoup.parse(webPage);
	// Elements links = doc.getElementsByClass("articleListContent");
	//
	// String newUrl, tryWebPage = "";
	// boolean found = false;
	// for (Element iter : links) {
	// if (!found) {
	// newUrl = iter.getElementsByTag("a").attr("href");
	// tryWebPage = HttpSupport.getInstance().getWebSite(PROVIDER_DOMAIN + newUrl);
	//
	// if (tryWebPage.contains(product.getEan())) {
	// found = true;
	// doc = Jsoup.parse(tryWebPage);
	//
	// price.setPrice(doc.getElementsByClass("price-container").get(0).getElementsByClass("current")
	// .get(0).ownText());
	// price.setSucessPerc(90);
	// return price;
	// }
	// }
	// }
	//
	// System.out.println("Page: " + tryWebPage);
	//
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// return null;
	// }

	@Override
	public String getProviderName() {
		return PROVIDER_NAME;
	}

}
