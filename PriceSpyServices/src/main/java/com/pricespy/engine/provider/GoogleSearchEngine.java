package com.pricespy.engine.provider;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.engine.web.HttpSupport;

public class GoogleSearchEngine  implements SearchEngine{

	private static final String PROVIDER_NAME = "Carrefour";
	private static final String PROVIDER_DOMAIN = "https://www.carrefour.es/";

	@Override
	public ProviderPrice getPrice(Product product) {
		//https://www.carrefour.es/global/?Ntt=8806088961361
		
		ProviderPrice price = new ProviderPrice();
		String providerDomain = "https://www.google.es/";
		try {
			// firefoxEngine();
			// chromeEngine();
			chromeRemoteEngine();
	        
//			String url = providerDomain + "global/?Ntt="+product.getEAN();
//			String webPage = HttpSupport.getInstance().getWebSite(url);
//			//System.out.println("Page: " + webPage);
//			
//			//https://www.carrefour.es/lavavajillas-samsung-waterwall-dw60m9550fw-blanco/VC4A-3215792/p
//			// Tengo el producto en la página anterior.
//			Document doc = Jsoup.parse(webPage);
//			//data-total-num-recs debe ser 1 (1 resultado en la busqueda) window.document.getElementsByClassName("result-list")
//			Elements resultList = doc.getElementsByClass("result-list");
//			if(resultList.size()>0) {
//				if("1".equals(resultList.get(0).attr("data-total-num-recs"))) {
//					return resultList.get(0).getElementsByClass("precio-nuevo").get(0).ownText();
//				}
//			}
			
			return price;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return price;
	}
	
	public static void chromeEngine() {
		
		System.setProperty("webdriver.chrome.driver", "/home/altamira/git/restatmirra/PriceSpy/selenium/Engines/chromedriver");
		
		String providerDomain = "https://www.google.es/";
		try {
			//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			//capabilities.setCapability("marionette", true);
			
			//WebDriver driver = new RemoteWebDriver(capabilities);
			WebDriver driver = new ChromeDriver();
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.get(providerDomain);
			
			// Find the text input element by its name
	        WebElement element = driver.findElement(By.name("q"));

	        // Enter something to search for
	        element.sendKeys("Cheese!");

	        // Now submit the form. WebDriver will find the form for us from the element
	        element.submit();

	        // Check the title of the page
	        System.out.println("Page title is1: " + driver.getTitle());
	        
	        // Google's search is rendered dynamically with JavaScript.
	        // Wait for the page to load, timeout after 10 seconds
//	        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
//	            public Boolean apply(WebDriver d) {
//	                return d.getTitle().toLowerCase().startsWith("cheese!");
//	            }
//	        });
	        //final WebDriverWait wait = new WebDriverWait(driver, 5);
	        //wait.until(ExpectedConditions.elementToBeClickable(By.id("someid")));
	        

	        // Should see: "cheese! - Google Search"
	        System.out.println("Page title is2: " + driver.getTitle());
	        
	        //Close the browser
	        driver.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// DONT WORK.
	public static void chromeRemoteEngine() {
		
		System.setProperty("webdriver.chrome.driver", "/home/altamira/git/restatmirra/PriceSpy/selenium/Engines/chromedriver");
		
		String providerDomain = "https://www.google.es/";
		try {
			//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			//capabilities.setCapability("marionette", true);
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			//capabilities.setBrowserName("chrome.switches");
			
			WebDriver driver = new RemoteWebDriver(new URL("http://localhost:9515"), capabilities);
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.get(providerDomain);
			
			// Find the text input element by its name
	        WebElement element = driver.findElement(By.name("q"));

	        // Enter something to search for
	        element.sendKeys("Cheese!");

	        // Now submit the form. WebDriver will find the form for us from the element
	        element.submit();

	        // Check the title of the page
	        System.out.println("Page title is1: " + driver.getTitle());
	        
	        // Google's search is rendered dynamically with JavaScript.
	        // Wait for the page to load, timeout after 10 seconds
//	        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
//	            public Boolean apply(WebDriver d) {
//	                return d.getTitle().toLowerCase().startsWith("cheese!");
//	            }
//	        });

	        // Should see: "cheese! - Google Search"
	        System.out.println("Page title is2: " + driver.getTitle());
	        
	        //Close the browser
	        driver.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void firefoxEngine() {
		
		System.setProperty("webdriver.gecko.driver", "/home/altamira/git/restatmirra/PriceSpy/selenium/Engines/geckodriver");
		
		String providerDomain = "https://www.google.es/";
		try {
			//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			//capabilities.setCapability("marionette", true);
			
			//WebDriver driver = new RemoteWebDriver(capabilities);
			WebDriver driver = new FirefoxDriver();
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.get(providerDomain);
			
			// Find the text input element by its name
	        WebElement element = driver.findElement(By.name("q"));

	        // Enter something to search for
	        element.sendKeys("Cheese!");

	        // Now submit the form. WebDriver will find the form for us from the element
	        element.submit();

	        // Check the title of the page
	        System.out.println("Page title is1: " + driver.getTitle());
	        
	        // Google's search is rendered dynamically with JavaScript.
	        // Wait for the page to load, timeout after 10 seconds
//	        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
//	            public Boolean apply(WebDriver d) {
//	                return d.getTitle().toLowerCase().startsWith("cheese!");
//	            }
//	        });

	        // Should see: "cheese! - Google Search"
	        System.out.println("Page title is2: " + driver.getTitle());
	        
	        //Close the browser
	        driver.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getProviderName() {
		return PROVIDER_NAME;
	}

}
