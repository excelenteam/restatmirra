package com.pricespy.engine.provider;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pricespy.bean.Product;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.engine.web.HttpSupport;

public class MediaMarktSearchEngine implements SearchEngine {

	private static final String PROVIDER_NAME = "Media Markt";
	private static final String PROVIDER_DOMAIN = "https://www.mediamarkt.es";

	@Override
	public ProviderPrice getPrice(Product product) {

		ProviderPrice price = new ProviderPrice();
		try {
			// https://api.empathybroker.com/search/v1/query/mediamarkt/search?jsonCallback=angular.callbacks._0&lang=es&origin=linked&q=Saivod+LVT51
			String url = "https://api.empathybroker.com/search/v1/query/mediamarkt/search?jsonCallback=angular.callbacks._0&lang=es&origin=linked&q="
					+ product.getBrand() + "+" + product.getModel();
			String webPage = HttpSupport.getInstance().getWebSite(url);
			String jsonRequest = webPage.substring(webPage.indexOf("(") + 1, webPage.length() - 2);
			// System.out.println("Page: " + jsonRequest);

			JsonObject jsonObject = new JsonParser().parse(jsonRequest).getAsJsonObject();
			JsonArray listInfo = null;
			JsonObject iter = null;
			String tryWebPage = null, tempPrice = null;
			boolean found = false;

			// System.out.println(jsonObject.get("content").getAsString()); //John
			if (jsonObject.get("content").getAsJsonObject().get("numFound").getAsInt() != 0) {
				listInfo = jsonObject.get("content").getAsJsonObject().get("docs").getAsJsonArray();

				for (int i = 0; i < listInfo.size(); i++) {
					iter = listInfo.get(i).getAsJsonObject();
					tempPrice = iter.get("price").getAsString();
					tryWebPage = HttpSupport.getInstance().getWebSite(iter.get("url").getAsString());
					// System.out.println("TryURL: " + tryWebPage);

					if (tryWebPage.contains(product.getEan())) {
						price.setPrice(iter.get("price").getAsString());
						price.setSucessPerc(90);
						return price;
					}
				}

				System.out
						.println("No ha encontrado el EAN aunque ha encontrado resultados. Se escoge el primer precio");
				price.setPrice(tempPrice);
				price.setSucessPerc(30);
				return price;
			}

			// No se han encontrado productos
			// if(!webPage.contains("No se han encontrado productos")) {
			// https://tiendas.mediamarkt.es/p/lavavajillas-bosch-serie-4-sms46mi08e-14-1366702
			// product-name
			// Document doc = Jsoup.parse(webPage);
			// Elements links = doc.getElementsByClass("product-name");
			//
			// String newUrl, tryWebPage = "";
			// boolean found = false;
			// for(Element iter : links) {
			// if(!found) {
			// newUrl = iter.getElementsByTag("a").attr("href");
			// tryWebPage = HttpSupport.getInstance().getWebSite(providerDomain +newUrl);
			//
			// if(tryWebPage.contains(product.getEAN())) {
			// found = true;
			// doc = Jsoup.parse(tryWebPage);
			//
			// return
			// doc.getElementsByClass("price-container").get(0).getElementsByClass("current").get(0).ownText();
			// }
			// }
			// }
			// }

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return price;
	}

	@Override
	public String getProviderName() {
		return PROVIDER_NAME;
	}

}
