package com.pricespy.engine.provider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.engine.web.HttpSupport;

public class CorteInglesSearchEngine implements SearchEngine {

	private static final String PROVIDER_NAME = "Corte Ingles";
	private static final String PROVIDER_DOMAIN = "https://www.elcorteingles.es";

	@Override
	public ProviderPrice getPrice(Product product) {

		ProviderPrice price = new ProviderPrice();
		try {
			String url = PROVIDER_DOMAIN + "/search/?s=" + product.getProduct() + "+" + product.getBrand() + "+"
					+ product.getModel();
			String webPage = HttpSupport.getInstance().getWebSite(url);
			// System.out.println("Page: " + webPage);

			// https://www.elcorteingles.es/electrodomesticos/A11611548-lavavajillas-saivod-lvt51-con-programa-de-media-carga/
			// product-name
			Document doc = Jsoup.parse(webPage);
			Elements links = doc.getElementsByClass("product-name");

			String newUrl, tryWebPage = "";
			boolean found = false;
			for (Element iter : links) {
				if (!found) {
					newUrl = iter.getElementsByTag("a").attr("href");
					tryWebPage = HttpSupport.getInstance().getWebSite(PROVIDER_DOMAIN + newUrl);

					if (tryWebPage.contains(product.getEan())) {
						found = true;
						doc = Jsoup.parse(tryWebPage);

						price.setPrice(doc.getElementsByClass("price-container").get(0).getElementsByClass("current")
								.get(0).ownText());
						price.setSucessPerc(90);
						return price;
					}
				}
			}

			// System.out.println("Page: " + tryWebPage);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return price;
	}

	@Override
	public String getProviderName() {
		return PROVIDER_NAME;
	}

}
