package com.pricespy.engine.provider;

import java.time.Duration;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.utils.DateUtils;

public interface SearchEngine {

	static final char COLON = ':';

	default ProviderPrice getTimedPrice(Product product) {
		Instant start = Instant.now();
		String processTime;
		ProviderPrice provPrice = new ProviderPrice();
		provPrice.setProviderName(getProviderName());
		try {
			provPrice = getPrice(product);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			provPrice.setProviderProcessError("Message:" + e.getMessage() + " Loc:" + e.getLocalizedMessage());
		}

		Instant end = Instant.now();
		processTime = DateUtils.getDurationFormatting(Duration.between(start, end));
		LogHolder.logger.info(getProviderName() + COLON + processTime);
		provPrice.setProviderProcessTime(processTime);

		return provPrice;
	}

	public ProviderPrice getPrice(Product product) throws Exception;

	public String getProviderName();
}

final class LogHolder { // not public
	static final Logger logger = LoggerFactory.getLogger(SearchEngine.class);
}
