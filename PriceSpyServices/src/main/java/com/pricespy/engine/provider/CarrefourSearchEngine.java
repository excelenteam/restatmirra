package com.pricespy.engine.provider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.engine.web.HttpSupport;

public class CarrefourSearchEngine implements SearchEngine {

	private static final String PROVIDER_NAME = "Carrefour";
	private static final String PROVIDER_DOMAIN = "http://www.carrefour.es/";

	@Override
	public ProviderPrice getPrice(Product product) throws Exception {
		// https://www.carrefour.es/global/?Ntt=8806088961361

		ProviderPrice price = new ProviderPrice();

		String url = PROVIDER_DOMAIN + "global/?Ntt=" + product.getEan();
		String webPage = HttpSupport.getInstance().getWebSite(url);
		// System.out.println("Page: " + webPage);

		// https://www.carrefour.es/lavavajillas-samsung-waterwall-dw60m9550fw-blanco/VC4A-3215792/p
		// Tengo el producto en la página anterior.
		Document doc = Jsoup.parse(webPage);
		// data-total-num-recs debe ser 1 (1 resultado en la busqueda)
		// window.document.getElementsByClassName("result-list")
		Elements resultList = doc.getElementsByClass("result-list");
		if (resultList.size() > 0) {
			if ("1".equals(resultList.get(0).attr("data-total-num-recs"))) {
				price.setPrice(resultList.get(0).getElementsByClass("precio-nuevo").get(0).ownText());
				price.setSucessPerc(90);
				return price;
			}
		}

		return price;
	}

	@Override
	public String getProviderName() {
		return PROVIDER_NAME;
	}

}
