package com.pricespy.engine;

import org.springframework.stereotype.Repository;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.engine.provider.SearchEngine;

@Repository
public class PriceSearchEngine {

	public ProviderPrice getPrice(SearchEngine engineSpecific, Product product) throws Exception {
		return engineSpecific.getTimedPrice(product);
	}

	// public ProviderPrice getPriceCorteIngles(Product product) {
	// return CorteInglesSearchEngine.getPrice(product);
	// }
	//
	// public ProviderPrice getPriceMediaMarkt(Product product) {
	// return MediaMarktSearchEngine.getPrice(product);
	// }
	//
	// public ProviderPrice getPriceKyeroo(Product product) {
	// return KyerooSearchEngine.getPrice(product);
	// }
	//
	// public ProviderPrice getPricePCComponentes(Product product) {
	// return PCComponentesSearchEngine.getPrice(product);
	// }
	//
	// public ProviderPrice getPriceMiElectro(Product product) {
	// return MiElectroSearchEngine.getPrice(product);
	// }
	//
	// public ProviderPrice getPriceCarrefour(Product product) {
	// return CarrefourSearchEngine.getPrice(product);
	// }
	//
	// public ProviderPrice getPriceGoogle(Product product) {
	// return GoogleSearchEngine.getPrice(product);
	// }

}
