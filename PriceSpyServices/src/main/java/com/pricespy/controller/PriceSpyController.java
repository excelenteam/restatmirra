package com.pricespy.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.seleniumhq.jetty9.server.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProductPrices;
import com.pricespy.service.PriceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@RestController
@RequestMapping("/priceService/v1")
@Api(value = "priceService", produces = "application/json", tags = "Prices", description = "Prices")
public class PriceSpyController {

	private static final Logger logger = LoggerFactory.getLogger(PriceSpyController.class);

	@Autowired
	private PriceService priceService;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "test/prices", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "TEST. Allways return the same prices", notes = "Return prices by product information")
	public @ResponseBody ProductPrices getPricesTest(HttpServletResponse response, HttpServletRequest request,
			Product product) {

		logger.info("Call with product: " + product);

		ProductPrices prices = priceService.getPricesByProductTest(product);
		return prices;

	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "prices", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get prices by product information", notes = "Return prices by product information")
	public @ResponseBody ProductPrices getPrices(HttpServletResponse response, HttpServletRequest request,
			Product product) {

		logger.info("Call with product: " + product);

		ProductPrices prices = null;
		try {
			prices = priceService.getPricesByProduct(product);
		} catch (Exception e) {
			// e.printStackTrace();
			response.setStatus(Response.SC_EXPECTATION_FAILED);
			prices.setWarnings(org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(e));
		}
		return prices;

	}

}
