package com.pricespy.launchers;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TestCase {

	@Test
	public void test() {

		RestTemplate restTemplate = new RestTemplate();

		String consumeJSONString = restTemplate.getForObject(
				"http://localhost:8080/priceService/v1/prices?brand=Bosch&model=SMS46MI08E&ean=4242002992006",
				String.class);
		// Discos Duros,Samsung,MZ-75E500B/EU,8806086523035

		JsonParser jsonParser = new JsonParser();
		JsonObject object = (JsonObject) jsonParser.parse(consumeJSONString).getAsJsonObject();

		JsonArray arrayProv = (JsonArray) object.get("pricesList");
		JsonObject element;
		String textOut;
		int i = 1;
		for (JsonElement prov : arrayProv) {
			element = prov.getAsJsonObject();
			textOut = "Prov " + StringUtils.rightPad(element.get("providerName").getAsString(), 10) + ":";
			if (!element.get("providerID").isJsonNull())
				textOut += " ID=" + element.get("providerID");
			if (!element.get("providerProcessTime").isJsonNull())
				textOut += " \tTime=" + element.get("providerProcessTime");
			// if (!element.get("providerName").isJsonNull())
			// textOut += " Name=" + element.get("providerName");
			if (!element.get("providerPrice").isJsonNull())
				textOut += " \tProvPrice=" + element.get("providerPrice");
			if (!element.get("price").isJsonNull())
				textOut += " \tPrice=" + element.get("price");
			if (!element.get("sucessPerc").isJsonNull())
				textOut += " \tPercent=" + element.get("sucessPerc");

			System.out.println(textOut);
		}
		System.out.println("All processes: " + object.get("processTime"));
	}

}