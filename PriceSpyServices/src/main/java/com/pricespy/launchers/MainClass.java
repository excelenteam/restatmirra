package com.pricespy.launchers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.pricespy.bean.Product;
import com.pricespy.bean.ProviderPrice;
import com.pricespy.engine.PriceSearchEngine;
import com.pricespy.engine.provider.AmazonSearchEngine;

public class MainClass {

	private static PriceSearchEngine engine;
	private static ClassLoader classloader;
	private static FileReader fileReader;

	public static void init() {
		engine = new PriceSearchEngine();
		classloader = Thread.currentThread().getContextClassLoader();
	}

	public static void main(String[] args) {
		init();
		// test1();
		test2();

		// Instant start = Instant.now();
		// Instant end = start.plusMillis(15015);
		//
		// Duration dur = Duration.between(start, end);
		// long s = dur.getSeconds();
		// String textDur = String.format("%d:%02d:%02d", s / 3600, (s % 3600) / 60, (s
		// % 60));
		//
		// System.out.println("Duration: " + textDur);

		// product.setArticle("Lavavajillas").setBrand("Saivod").setModel("LVT51").setEAN("2047104503721");

		// String price = engine.getPrice(product);
	}

	private static void test1() {

		loadFile("first_version.csv");
		String line = "";
		String cvsSplitBy = ",";
		int numFile = 0;

		try (BufferedReader br = new BufferedReader(fileReader)) {
			while ((line = br.readLine()) != null) {
				numFile++;
				if (numFile == 1 && line.contains("EAN")) {
					// cabecera
				} else {
					// use comma as separator
					String[] productData = line.split(cvsSplitBy);
					Product product = new Product();
					// product.setArticle(productData[0]).setBrand(productData[1]).setModel(productData[2]).setEAN(productData[3]);
					// System.out.println("Country [code= " + country[4] + " , name=" + country[5] +
					// "]");

					// String price = engine.getPriceCarrefour(product);
					// String price = engine.getPriceCorteIngles(product);
					// String price = engine.getPriceKyeroo(product);
					// String price = engine.getPriceMediaMarkt(product);
					// String price = engine.getPriceMiElectro(product);

					// String price = engine.getPricePCComponentes(product);
					// String price = engine.getPriceGoogle(product);
					// System.out.println("Precio "+numFile+": " + price);
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void test2() {

		try {
			Product product = new Product();
			product.setProduct("Discos duros");
			product.setBrand("Samsung");
			product.setModel("MZ-75E500B/EU");
			product.setEan("8806086523035");
			ProviderPrice provPrice;

			// provPrice = engine.getPrice(new PCComponentesSearchEngine(), product);
			provPrice = engine.getPrice(new AmazonSearchEngine(), product);

			System.out.println(provPrice);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// product.setArticle(productData[0]).setBrand(productData[1]).setModel(productData[2]).setEAN(productData[3]);
		// System.out.println("Country [code= " + country[4] + " , name=" + country[5] +
		// "]");

		// String price = engine.getPriceCarrefour(product);
		// String price = engine.getPriceCorteIngles(product);
		// String price = engine.getPriceKyeroo(product);
		// String price = engine.getPriceMediaMarkt(product);
		// String price = engine.getPriceMiElectro(product);

		// String price = engine.getPricePCComponentes(product);
		// String price = engine.getPriceGoogle(product);
		// System.out.println("Precio "+numFile+": " + price);

	}

	private static void loadFile(String csvFile) {

		try {
			fileReader = new FileReader("resource/" + csvFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// try (BufferedReader br = new BufferedReader(new FileReader("resource/" +
		// csvFile))) {
		//
		// buffrea = br;
		//
		//
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
	}
}
