package com.pricespy.launchers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
//@ComponentScan(basePackageClasses = PriceSpyController.class)
@ComponentScan(basePackages = {"com.pricespy", "package.of.RmiContext"})
public class LaunchAppDev {

	private static final Logger logger = LoggerFactory.getLogger(LaunchAppDev.class);

	public static void main(String[] args) {
		SpringApplication.run(LaunchAppDev.class, args);

	    logger.error("Message logged at ERROR level");
	    logger.warn("Message logged at WARN level");
	    logger.info("Message logged at INFO level");
	    logger.debug("Message logged at DEBUG level");
	}
}
