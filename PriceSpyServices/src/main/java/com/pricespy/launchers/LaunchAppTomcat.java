package com.pricespy.launchers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.pricespy", "package.of.RmiContext" })
@EnableAutoConfiguration
public class LaunchAppTomcat extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(LaunchAppTomcat.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(LaunchAppTomcat.class, args);
	}
}
