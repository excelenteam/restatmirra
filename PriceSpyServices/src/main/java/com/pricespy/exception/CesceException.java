package com.pricespy.exception;


public class CesceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CesceException() {
		super("error.message");
	}

	public CesceException(String message) {
		super(message);
	}

	public CesceException(Throwable throwable, String message) {
		super(message, throwable);

	}

}
