package com.pricespy.repository.impl;

import com.pricespy.bean.CustomerQueryParams;
import com.pricespy.bean.ProductPrices;
import com.pricespy.repository.PriceRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class PriceRepositoryMock implements PriceRepository {

	@Override
	public ProductPrices getDossierByID(Integer customerCode, String languageCode, Integer dossierNumber) {
		return new ProductPrices();
	}

	@Override
	public ProductPrices getClassificationSupplements(Integer customerCode, String languageCode, Integer contractNumber,
			Integer suppNumber, Integer debtorCode, String originId, String customerUserCode, Integer dossierNumber) {
		// TODO Auto-generated method stub
		return new ProductPrices();
	}

	@Override
	public List<ProductPrices> getListDossiers(Integer customerCode, String languageCode, String situationCode,
			CustomerQueryParams custParams, Integer dossierNum, Integer supplementNum, String debtorName,
			String debtorCountryCode, String debtorFiscalCode, String referenceCode) {
		return new ArrayList<ProductPrices>();
	}
	
	@Override
	public List<String> getDossiersManagerRecovery(Integer customerCode, String languageCode, String dossierNum,
			CustomerQueryParams custParams) {
		return new ArrayList<String>();
	}
}
