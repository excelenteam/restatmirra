package com.pricespy.repository;

import java.util.List;

import com.pricespy.bean.CustomerQueryParams;
import com.pricespy.bean.ProductPrices;

public interface PriceRepository {

	// List<NonPayments> findAll();

	// cambiar por NonPayment
	ProductPrices getDossierByID(Integer customerCode, String languageCode, Integer dossierNumber);

	ProductPrices getClassificationSupplements(Integer customerCode, String languageCode, Integer contractNumber,
			Integer suppNumber, Integer debtorCode, String originId, String customerUserCode, Integer dossierNumber);

	public List<ProductPrices> getListDossiers(Integer customerCode, String languageCode, String situationCode,
			CustomerQueryParams custParams, Integer dossierNum, Integer supplementNum, String debtorName,
			String debtorCountryCode, String debtorFiscalCode, String referenceCode);

	public List<String> getDossiersManagerRecovery(Integer customerCode, String languageCode, String dossierNum,
			CustomerQueryParams custParams);
}
