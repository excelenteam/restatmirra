package com.pricespy.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "ProviderPrice", description = "Price of a provider")
public class ProviderPrice {

	@ApiModelProperty(position = 1, value = "Identifier of provider", example = "1")
	private Integer providerID;

	@ApiModelProperty(position = 2, value = "Name of provider", example = "name of provider")
	private String providerName;

	@ApiModelProperty(position = 3, value = "Price obtained", example = "100.55")
	private Double providerPrice;

	@ApiModelProperty(position = 4, value = "Price obtained", example = "100.55")
	private String price;

	@ApiModelProperty(position = 5, value = "Sucess per cent", example = "55")
	private Integer sucessPerc;

	@ApiModelProperty(position = 6, value = "Provider process time", example = "24:59:59")
	private String providerProcessTime;

	@ApiModelProperty(position = 7, value = "Provider process error")
	private String providerProcessError;

	public Integer getProviderID() {
		return providerID;
	}

	public void setProviderID(Integer providerID) {
		this.providerID = providerID;
	}

	public ProviderPrice withProviderID(Integer providerID) {
		this.providerID = providerID;
		return this;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public ProviderPrice withProviderName(String providerName) {
		this.providerName = providerName;
		return this;
	}

	public Double getProviderPrice() {
		return providerPrice;
	}

	public void setProviderPrice(Double providerPrice) {
		this.providerPrice = providerPrice;
	}

	public ProviderPrice withProviderPrice(Double providerPrice) {
		this.providerPrice = providerPrice;
		return this;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Integer getSucessPerc() {
		return sucessPerc;
	}

	public void setSucessPerc(Integer sucessPerc) {
		this.sucessPerc = sucessPerc;
	}

	public ProviderPrice withSucessPerc(Integer sucessPerc) {
		this.sucessPerc = sucessPerc;
		return this;
	}

	public String getProviderProcessTime() {
		return providerProcessTime;
	}

	public void setProviderProcessTime(String providerProcessTime) {
		this.providerProcessTime = providerProcessTime;
	}

	public ProviderPrice withProviderProcessTime(String providerProcessTime) {
		this.providerProcessTime = providerProcessTime;
		return this;
	}

	public String getProviderProcessError() {
		return providerProcessError;
	}

	public void setProviderProcessError(String providerProcessError) {
		this.providerProcessError = providerProcessError;
	}

}
