package com.pricespy.bean;

import java.math.BigDecimal;

import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

@ApiModel(value = "Product", description = "Product for search")
public class Product {
	
	@ApiModelProperty(value = "Kind of product", example = "Lavavajillas", required = false)
	private String product;
	
	@ApiModelProperty(value = "Brand of product", example = "Bosch", required = true)
	private String brand;
	
	@ApiModelProperty(value = "Model of product", example = "SMS25AW05E", required = true)
	private String model;
	
	@ApiModelProperty(value = "Sucess per cent", example = "4242002996967", required = true)
	private String ean;

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brand == null) ? 0 : brand.hashCode());
		result = prime * result + ((ean == null) ? 0 : ean.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Product [product=" + product + ", brand=" + brand + ", model=" + model + ", ean=" + ean + "]";
	}

}
