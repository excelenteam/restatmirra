package com.pricespy.bean;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "ProductPrices", description = "Prices of the product")
public class ProductPrices {
	
	@ApiModelProperty(position = 1, value = "Prices list")
	private List<ProviderPrice> pricesList;
	
	@ApiModelProperty(position = 2, value = "Warning messages", example = "Could be a mistake")
	private String warnings;

	@ApiModelProperty(position = 3, value = "Process time", example = "24:59:59")
	private String processTime;

	public List<ProviderPrice> getPricesList() {
		return pricesList;
	}

	public void setPricesList(List<ProviderPrice> pricesList) {
		this.pricesList = pricesList;
	}

	public void addProviderPrice(ProviderPrice providerPrice) {
		if(this.pricesList == null)
			this.pricesList = new LinkedList<ProviderPrice>();
		this.pricesList.add(providerPrice);
	}

	public String getWarnings() {
		return warnings;
	}

	public void setWarnings(String warnings) {
		this.warnings = warnings;
	}

	public String getProcessTime() {
		return processTime;
	}

	public void setProcessTime(String processTime) {
		this.processTime = processTime;
	}
	
//	@ApiModelProperty(position = 1, value = "Policy mode", example = "677890200", required = true)
//	private BigDecimal contractNumber;
//	
//	@ApiModelProperty(position = 2, value = "File number", example = "66781111", required = true)
//	private BigDecimal fileNumber;
//	
//	@ApiModelProperty(position = 3, value = "Supplement number", example = "555534", required = true)
//	private BigDecimal suppNumber;
//	
//	@ApiModelProperty(position = 5, value = "Description of the modality", example = "basic modality", required = true)
//	private String descModality;
//	
//	@ApiModelProperty(position = 6, value = "Insured code", example = "600012345", required = true)
//	private BigDecimal insCode;
//	
//	@ApiModelProperty(position = 7, value = "Country code of the policyholder", example = "180", required = true)
//	private BigDecimal couCodePolicyHolder;
//	
//	@ApiModelProperty(position = 8, value = "Description of the policyholder's country", example = "European country", required = true)
//	private String descCounCodePolicyHolder;
//	
//	@ApiModelProperty(position = 9, value = "Company code", example = "B", required = true)
//	private BigDecimal compCode;
//	
//	@ApiModelProperty(position = 10, value = "Company description", example = "IT company", required = true)
//	private String descCompany;
//	
//	//cod divisa expediente
//	@ApiModelProperty(position = 12, value = "File currency code(ISO format)", example = "XYC", required = true)
//	private String fileCurrCode;
//	
//	@ApiModelProperty(position = 12, value = "Description of the file currency", example = "ABV", required = true)
//	private String descFileCurrency;
}
