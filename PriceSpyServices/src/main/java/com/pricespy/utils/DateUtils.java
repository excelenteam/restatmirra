package com.pricespy.utils;

import java.time.Duration;

public class DateUtils {
	
	public static String getDurationFormatting(Duration duration) {
		long s = duration.getSeconds();
		return String.format("%d:%02d:%02d", s / 3600, (s % 3600) / 60, (s % 60));
	}
}
