package com.pricespy.engine.provider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pricespy.bean.Product;
import com.pricespy.engine.web.HttpSupport;

public class MiElectroSearchEngine {

	public static String getPrice(Product product) {
		
		String providerDomain = "https://www.mielectro.es";
		try {
			//https://www.kyeroo.com/catalogsearch/result/?q=4242002992006
			String url = providerDomain + "/busqueda/?c=&q="+product.getBrand()+"+"+product.getModel();
			String webPage = HttpSupport.getInstance().getWebSite(url);

			//System.out.println("Page: " + webPage);
			
			//https://www.kyeroo.com/electrodomesticos/lavavajillas/lavavajillas-bosch-serie-4-sms46mi08e-independiente-14cubiertos-a-lavavajilla.html
			// product-name
			String tryWebPage = null, newUrl, priceCurrency;
			if(webPage.contains("contentResultados")) {
				//System.out.println("Pagina("+webPage+")");
				Document doc = Jsoup.parse(webPage);
				
				Elements resultList = doc.getElementsByClass("items").get(0).getElementsByClass("colTxt");
				// Añade lo siguiente para verificar:
				//href: /bosch-lavavajillas-bosch-sms25aw05e---a-12-servicios-7-progr-motor-ecosilence-display-inox-1085071/
				//if(resultList.size() > 1) {
				
				Element priceElement = null;
				for(Element iter : resultList){
					newUrl = iter.getElementsByTag("a").attr("href");
					tryWebPage = HttpSupport.getInstance().getWebSite(providerDomain + newUrl);
					
					if(tryWebPage.contains(product.getEAN())) {
						priceElement = iter.getElementsByClass("precio-mielectro-blanco-mosaico").get(0);
						priceCurrency = priceElement.ownText();
						priceCurrency += priceElement.child(0).ownText();
						return priceCurrency.substring(0, priceCurrency.length() - 1 ) ;						
					}
				}
			}
			
			return null;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
