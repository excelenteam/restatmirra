package com.pricespy.engine.provider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pricespy.bean.Product;
import com.pricespy.engine.web.HttpSupport;

public class CarrefourSearchEngine {

	public static String getPrice(Product product) {
		//https://www.carrefour.es/global/?Ntt=8806088961361
		
		
		String providerDomain = "https://www.carrefour.es/";
		try {
			String url = providerDomain + "global/?Ntt="+product.getEAN();
			String webPage = HttpSupport.getInstance().getWebSite(url);
			//System.out.println("Page: " + webPage);
			
			//https://www.carrefour.es/lavavajillas-samsung-waterwall-dw60m9550fw-blanco/VC4A-3215792/p
			// Tengo el producto en la página anterior.
			Document doc = Jsoup.parse(webPage);
			//data-total-num-recs debe ser 1 (1 resultado en la busqueda) window.document.getElementsByClassName("result-list")
			Elements resultList = doc.getElementsByClass("result-list");
			if(resultList.size()>0) {
				if("1".equals(resultList.get(0).attr("data-total-num-recs"))) {
					return resultList.get(0).getElementsByClass("precio-nuevo").get(0).ownText();
				}
			}
			
			return null;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
