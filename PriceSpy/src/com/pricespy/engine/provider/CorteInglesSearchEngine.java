package com.pricespy.engine.provider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pricespy.bean.Product;
import com.pricespy.engine.web.HttpSupport;

public class CorteInglesSearchEngine {

	public static String getPrice(Product product) {
		
		String providerDomain = "https://www.elcorteingles.es";
		try {
			String url = providerDomain + "/electrodomesticos/search/?s="+product.getArticle()+"+"+product.getBrand()+"+"+product.getModel();
			String webPage = HttpSupport.getInstance().getWebSite(url);
			//System.out.println("Page: " + webPage);
			
			//https://www.elcorteingles.es/electrodomesticos/A11611548-lavavajillas-saivod-lvt51-con-programa-de-media-carga/
			// product-name
			Document doc = Jsoup.parse(webPage);
			Elements links = doc.getElementsByClass("product-name");
			
			String newUrl, tryWebPage = "";
			boolean found = false;
			for(Element iter : links) {
				if(!found) {
					newUrl = iter.getElementsByTag("a").attr("href");
					tryWebPage = HttpSupport.getInstance().getWebSite(providerDomain +newUrl);
					
					if(tryWebPage.contains(product.getEAN())) {
						found = true;
						doc = Jsoup.parse(tryWebPage);
						
						return doc.getElementsByClass("price-container").get(0).getElementsByClass("current").get(0).ownText();
					}
				}
			}
			
			System.out.println("Page: " + tryWebPage);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
