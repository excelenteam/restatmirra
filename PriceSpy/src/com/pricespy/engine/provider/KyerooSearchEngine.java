package com.pricespy.engine.provider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.pricespy.bean.Product;
import com.pricespy.engine.web.HttpSupport;

public class KyerooSearchEngine {

	public static String getPrice(Product product) {
		
		String providerDomain = "https://www.kyeroo.com";
		try {
			//https://www.kyeroo.com/catalogsearch/result/?q=4242002992006
			String url = providerDomain + "/catalogsearch/result/?q="+product.getEAN();
			String webPage = HttpSupport.getInstance().getWebSite(url);
			String priceCurrency;
			//System.out.println("Page: " + webPage);
			
			//https://www.kyeroo.com/electrodomesticos/lavavajillas/lavavajillas-bosch-serie-4-sms46mi08e-independiente-14cubiertos-a-lavavajilla.html
			// product-name
			
			if(webPage.contains("\"products-list")) {
				//System.out.println("Pagina("+webPage+")");
				Document doc = Jsoup.parse(webPage);
				
				Elements resultList = doc.getElementsByClass("products-list").get(0).getElementsByTag("li");
				// Añade lo siguiente:
				//<li><a href="https://www.kyeroo.com/wishlist/index/add/product/5724/form_key/vtVBoIrg3suTw9JD/" class="link-wishlist btn" title="Agregar a la lista de artículos de interés"><i class="fa fa-heart"></i></a></li>
				//<li><a href="https://www.kyeroo.com/catalog/product_compare/add/product/5724/uenc/aHR0cHM6Ly93d3cua3llcm9vLmNvbS9jYXRhbG9nc2VhcmNoL3Jlc3VsdC8_cT00MjQyMDAyOTkyMDA2/form_key/vtVBoIrg3suTw9JD/" class="link-compare btn" title="Agregar a la lista para comparar"><i class="fa fa-retweet"></i></a></li>
				if(resultList.size() == 3) {
					// TODO Regular Expression
					priceCurrency = resultList.get(0).getElementsByClass("price").get(0).ownText();
					return priceCurrency.substring(0, priceCurrency.length() - 2 ) ;
				}
			}
			
			return null;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
