package com.pricespy.engine;

import com.pricespy.bean.Product;
import com.pricespy.engine.provider.*;

public class PriceSearchEngine {

	public String getPriceCorteIngles(Product product) {
		String price = CorteInglesSearchEngine.getPrice(product);
		return price;
	}
	
	public String getPriceMediaMarkt(Product product) {
		String price = MediaMarktSearchEngine.getPrice(product);
		return price;
	}
	
	public String getPriceKyeroo(Product product) {
		String price = KyerooSearchEngine.getPrice(product);
		return price;
	}
	
	public String getPricePCComponentes(Product product) {
		String price = PCComponentesSearchEngine.getPrice(product);
		return price;
	}
	
	public String getPriceMiElectro(Product product) {
		String price = MiElectroSearchEngine.getPrice(product);
		return price;
	}
	
	public String getPriceCarrefour(Product product) {
		String price = CarrefourSearchEngine.getPrice(product);
		return price;
	}
	
	public String getPriceGoogle(Product product) {
		String price = GoogleSearchEngine.getPrice(product);
		return price;
	}

}
