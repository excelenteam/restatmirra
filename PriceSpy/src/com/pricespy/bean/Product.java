package com.pricespy.bean;

public class Product {
	
	private String article;
	
	private String brand;
	
	private String model;
	
	private String EAN;

	public String getArticle() {
		return article;
	}

	public Product setArticle(String article) {
		this.article = article;
		return this;
	}

	public String getBrand() {
		return brand;
	}

	public Product setBrand(String brand) {
		this.brand = brand;
		return this;
	}

	public String getModel() {
		return model;
	}

	public Product setModel(String model) {
		this.model = model;
		return this;
	}

	public String getEAN() {
		return EAN;
	}

	public Product setEAN(String eAN) {
		EAN = eAN;
		return this;
	}
	
	
}
