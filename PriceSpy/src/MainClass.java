

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import com.pricespy.bean.Product;
import com.pricespy.engine.PriceSearchEngine;

public class MainClass {

	private static PriceSearchEngine engine;
	private static ClassLoader classloader;
	private static FileReader fileReader;
	
	public static void init() {
		engine = new PriceSearchEngine();
		classloader = Thread.currentThread().getContextClassLoader();
	}
	
	
	public static void main(String[] args) {
		init();
		test1();
		
		//product.setArticle("Lavavajillas").setBrand("Saivod").setModel("LVT51").setEAN("2047104503721");
		
		//String price = engine.getPrice(product);
	}

	private static void test1() {
		
		loadFile("first_version.csv");
		String line = "";
	    String cvsSplitBy = ",";
	    int numFile = 0;

		try (BufferedReader br = new BufferedReader(fileReader)) {
			while ((line = br.readLine()) != null) {
				numFile++;
				if(numFile==1 && line.contains("EAN")) {
					//cabecera
				}else {
				    // use comma as separator
				    String[] productData = line.split(cvsSplitBy);
				    Product product = new Product();
				    product.setArticle(productData[0]).setBrand(productData[1]).setModel(productData[2]).setEAN(productData[3]);
				    //System.out.println("Country [code= " + country[4] + " , name=" + country[5] + "]");
				    
				    //String price = engine.getPriceCarrefour(product);
				    //String price = engine.getPriceCorteIngles(product);
				    //String price = engine.getPriceKyeroo(product);
				    //String price = engine.getPriceMediaMarkt(product);
				    //String price = engine.getPriceMiElectro(product);
				    
// 	String price = engine.getPricePCComponentes(product);
				    String price = engine.getPriceGoogle(product);
				    System.out.println("Precio "+numFile+": " + price);
				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static void loadFile(String csvFile) {
	
		try {
			fileReader = new FileReader("resource/" + csvFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//	    try (BufferedReader br = new BufferedReader(new FileReader("resource/" + csvFile))) {
//	
//	    	buffrea = br;
//	        
//	
//	    } catch (IOException e) {
//	        e.printStackTrace();
//	    }
	}
}
