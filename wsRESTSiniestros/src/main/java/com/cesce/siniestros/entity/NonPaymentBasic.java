package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "NonPaymentBasic", description = "Basic data of a non-payment")
public class NonPaymentBasic {

	@ApiModelProperty(value = "Debtor's code", example = "25042017")
	private BigDecimal dateCommuniNonPayment;
	
	@ApiModelProperty(value = "Initial expiration date" , example = "25042017")
	private BigDecimal initExpirationDate;
	
	@ApiModelProperty(value = "Final expiration date" , example = "25062017")
	private BigDecimal finalExpirationDate;
	
	
	@ApiModelProperty(value = "NonPayment reason code" , example = "013")
	private BigDecimal nonReasonCode;
	
	
	@ApiModelProperty(value = "Date of reason for non-payment" , example = "12092017")
	private String nonReasonNonPayDate;


	public BigDecimal getDateCommuniNonPayment() {
		return dateCommuniNonPayment;
	}


	public NonPaymentBasic setDateCommuniNonPayment(BigDecimal dateCommuniNonPayment) {
		this.dateCommuniNonPayment = dateCommuniNonPayment;
		return this;
	}


	public BigDecimal getInitExpirationDate() {
		return initExpirationDate;
	}


	public NonPaymentBasic setInitExpirationDate(BigDecimal initExpirationDate) {
		this.initExpirationDate = initExpirationDate;
		return this;
	}


	public BigDecimal getFinalExpirationDate() {
		return finalExpirationDate;
	}


	public NonPaymentBasic setFinalExpirationDate(BigDecimal finalExpirationDate) {
		this.finalExpirationDate = finalExpirationDate;
		return this;
	}


	public BigDecimal getNonReasonCode() {
		return nonReasonCode;
	}


	public NonPaymentBasic setNonReasonCode(BigDecimal nonReasonCode) {
		this.nonReasonCode = nonReasonCode;
		return this;
	}


	public String getNonReasonNonPayDate() {
		return nonReasonNonPayDate;
	}


	public NonPaymentBasic setNonReasonNonPayDate(String nonReasonNonPayDate) {
		this.nonReasonNonPayDate = nonReasonNonPayDate;
		return this;
	}
	
	
}
