package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author agustin.rodriguez
 * @version 1.0 18 de Julio de 2017
 *
 * Liquidation data
 */
@ApiModel(value = "Recovery item from recovery list", description = "")
public class RecoveryItem {

	@ApiModelProperty(position = 1, value = "Recovery number", example = "12345678", required = true)
	private BigDecimal recoveryNum;
	
	@ApiModelProperty(position = 2, value = "Recovery date", example = "12345678", required = true)
	private BigDecimal recoveryDate;
	
	@ApiModelProperty(position = 3, value = "Description of payment origin", example = "123ABC..40", required = true)
	private String paymentOriginDes;
	
	@ApiModelProperty(position = 4, value = "Currency code", example = "123", required = true)
	private BigDecimal currencyCode;
	
	@ApiModelProperty(position = 5, value = "Currency code description", example = "12A", required = true)
	private String currencyDesc;
	
	@ApiModelProperty(position = 6, value = "Amount of total recovery", example = "15.3", required = true)
	private BigDecimal amoTotalRecovery;

	public BigDecimal getRecoveryNum() {
		return recoveryNum;
	}

	public RecoveryItem setRecoveryNum(BigDecimal recoveryNum) {
		this.recoveryNum = recoveryNum;
		return this;
	}

	public BigDecimal getRecoveryDate() {
		return recoveryDate;
	}

	public RecoveryItem setRecoveryDate(BigDecimal recoveryDate) {
		this.recoveryDate = recoveryDate;
		return this;
	}

	public String getPaymentOriginDes() {
		return paymentOriginDes;
	}

	public RecoveryItem setPaymentOriginDes(String paymentOriginDes) {
		this.paymentOriginDes = paymentOriginDes;
		return this;
	}

	public BigDecimal getCurrencyCode() {
		return currencyCode;
	}

	public RecoveryItem setCurrencyCode(BigDecimal currencyCode) {
		this.currencyCode = currencyCode;
		return this;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public RecoveryItem setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
		return this;
	}

	public BigDecimal getAmoTotalRecovery() {
		return amoTotalRecovery;
	}

	public RecoveryItem setAmoTotalRecovery(BigDecimal amoTotalRecovery) {
		this.amoTotalRecovery = amoTotalRecovery;
		return this;
	}
}
