package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Management", description = "Management")
public class Management {
	
	@ApiModelProperty(value = "Type of processing", example = "677890200")
	private String typeProcessing;
	
	
	@ApiModelProperty(value = "Description of the type of processing", example = "677890200")
	private String descTypeProcessing;
	
	
	@ApiModelProperty(value = "Location code", example = "123")
	private BigDecimal locationCode;
	
	@ApiModelProperty(value = "Location description", example = "Indian country")
	private String locDescription;
	
	@ApiModelProperty(value = "Processing status code", example = "234")
	private BigDecimal procStatusCode;
	
	@ApiModelProperty(value = "Processing status description", example = "example of processing status")
	private BigDecimal procStatusDesc;
	
	
	//Tramitador
	@ApiModelProperty(value = "Dealer code", example = "234")
	private String dealerCode;
	
	@ApiModelProperty(value = "Dealer name", example = "Juan Pérez")
	private String dealerName;
	
	//Recobrador
	@ApiModelProperty(value = "Recovery code", example = "234")
	private String recoveryCode;
	
	@ApiModelProperty(value = "Recovery name", example = "Manuel Pérez")
	private String recoveryName;
	//
	
	@ApiModelProperty(value = "non-payment reason", example = "001")
	private BigDecimal nonPaymentReason;
	
	@ApiModelProperty(value = "non-payment reason description", example = "No founds")
	private String nonPaymentReasonDesc;
	
	
	@ApiModelProperty(value = "Vertification level code", example = "001")
	private BigDecimal verLevelCode;
	
	@ApiModelProperty(value = "Opening date", example = "12122015")
	private BigDecimal openingDate;
	
	@ApiModelProperty(value = "Communication date", example = "12122015")
	private BigDecimal communicationDate;
	
	@ApiModelProperty(value = "Date of documentation provided", example = "12122015")
	private BigDecimal docProvidedDate;
	
	@ApiModelProperty(value = "Date of coverage decision", example = "12122015")
	private BigDecimal coverageDecSate;
	
	@ApiModelProperty(value = "Closing information")
	private Closing closing;
	
	@ApiModelProperty(value = "Fixed day for the payment of the debtor (1)", example = "12")
	private BigDecimal fixDayPaymentDebtor1;
	
	@ApiModelProperty(value = "Fixed day for the payment of the debtor (2)", example = "12")
	private BigDecimal fixDayPaymentDebtor2;
	
	
	@ApiModelProperty(value = "Indicator of termination of compensation", example = "C")
	private String indTerminationComp;
	
	//indicador de mercancía retenida
	@ApiModelProperty(value = "Goods indicator held", example = "C")
	private String goodsIndHeld;

	public String getTypeProcessing() {
		return typeProcessing;
	}

	public Management setTypeProcessing(String typeProcessing) {
		this.typeProcessing = typeProcessing;
		return this;
	}

	public String getDescTypeProcessing() {
		return descTypeProcessing;
	}

	public Management setDescTypeProcessing(String descTypeProcessing) {
		this.descTypeProcessing = descTypeProcessing;
		return this;
	}

	public BigDecimal getLocationCode() {
		return locationCode;
	}

	public Management setLocationCode(BigDecimal locationCode) {
		this.locationCode = locationCode;
		return this;
	}

	public String getLocDescription() {
		return locDescription;
	}

	public Management setLocDescription(String locDescription) {
		this.locDescription = locDescription;
		return this;
	}

	public BigDecimal getProcStatusCode() {
		return procStatusCode;
	}

	public Management setProcStatusCode(BigDecimal procStatusCode) {
		this.procStatusCode = procStatusCode;
		return this;
	}

	public BigDecimal getProcStatusDesc() {
		return procStatusDesc;
	}

	public Management setProcStatusDesc(BigDecimal procStatusDesc) {
		this.procStatusDesc = procStatusDesc;
		return this;
	}

	public String getDealerCode() {
		return dealerCode;
	}

	public Management setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
		return this;
	}

	public String getDealerName() {
		return dealerName;
	}

	public Management setDealerName(String dealerName) {
		this.dealerName = dealerName;
		return this;
	}

	public String getRecoveryCode() {
		return recoveryCode;
	}

	public Management setRecoveryCode(String recoveryCode) {
		this.recoveryCode = recoveryCode;
		return this;
	}

	public String getRecoveryName() {
		return recoveryName;
	}

	public Management setRecoveryName(String recoveryName) {
		this.recoveryName = recoveryName;
		return this;
	}

	public BigDecimal getNonPaymentReason() {
		return nonPaymentReason;
	}

	public Management setNonPaymentReason(BigDecimal nonPaymentReason) {
		this.nonPaymentReason = nonPaymentReason;
		return this;
	}

	public String getNonPaymentReasonDesc() {
		return nonPaymentReasonDesc;
	}

	public Management setNonPaymentReasonDesc(String nonPaymentReasonDesc) {
		this.nonPaymentReasonDesc = nonPaymentReasonDesc;
		return this;
	}

	public BigDecimal getVerLevelCode() {
		return verLevelCode;
	}

	public Management setVerLevelCode(BigDecimal verLevelCode) {
		this.verLevelCode = verLevelCode;
		return this;
	}

	public BigDecimal getOpeningDate() {
		return openingDate;
	}

	public Management setOpeningDate(BigDecimal openingDate) {
		this.openingDate = openingDate;
		return this;
	}

	public BigDecimal getCommunicationDate() {
		return communicationDate;
	}

	public Management setCommunicationDate(BigDecimal communicationDate) {
		this.communicationDate = communicationDate;
		return this;
	}

	public BigDecimal getDocProvidedDate() {
		return docProvidedDate;
	}

	public Management setDocProvidedDate(BigDecimal docProvidedDate) {
		this.docProvidedDate = docProvidedDate;
		return this;
	}

	public BigDecimal getCoverageDecSate() {
		return coverageDecSate;
	}

	public Management setCoverageDecSate(BigDecimal coverageDecSate) {
		this.coverageDecSate = coverageDecSate;
		return this;
	}

	public Closing getClosing() {
		return closing;
	}

	public Management setClosing(Closing closing) {
		this.closing = closing;
		return this;
	}

	public BigDecimal getFixDayPaymentDebtor1() {
		return fixDayPaymentDebtor1;
	}

	public Management setFixDayPaymentDebtor1(BigDecimal fixDayPaymentDebtor1) {
		this.fixDayPaymentDebtor1 = fixDayPaymentDebtor1;
		return this;
	}

	public BigDecimal getFixDayPaymentDebtor2() {
		return fixDayPaymentDebtor2;
	}

	public Management setFixDayPaymentDebtor2(BigDecimal fixDayPaymentDebtor2) {
		this.fixDayPaymentDebtor2 = fixDayPaymentDebtor2;
		return this;
	}

	public String getIndTerminationComp() {
		return indTerminationComp;
	}

	public Management setIndTerminationComp(String indTerminationComp) {
		this.indTerminationComp = indTerminationComp;
		return this;
	}

	public String getGoodsIndHeld() {
		return goodsIndHeld;
	}

	public Management setGoodsIndHeld(String goodsIndHeld) {
		this.goodsIndHeld = goodsIndHeld;
		return this;
	}
	
	
}
