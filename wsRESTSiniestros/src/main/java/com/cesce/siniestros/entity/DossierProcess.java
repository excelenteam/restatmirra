package com.cesce.siniestros.entity;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author agustin.rodriguez
 * @version 1.0 18 de Julio de 2017
 *
 * Liquidation data
 */
@ApiModel(value = "Manager of processing", description = "")
public class DossierProcess {
	
	@ApiModelProperty(position = 1, value = "Movement date", example = "12345678", required = true)
	private BigDecimal movementDate;
	
	@ApiModelProperty(position = 2, value = "Movement code", example = "123", required = true)
	private BigDecimal movementCode;
	
	@ApiModelProperty(position = 3, value = "Movement description", example = "123ABC..50", required = true)
	private String movementDes;

	public BigDecimal getMovementDate() {
		return movementDate;
	}

	public DossierProcess setMovementDate(BigDecimal movementDate) {
		this.movementDate = movementDate;
		return this;
	}

	public BigDecimal getMovementCode() {
		return movementCode;
	}

	public DossierProcess setMovementCode(BigDecimal movementCode) {
		this.movementCode = movementCode;
		return this;
	}

	public String getMovementDes() {
		return movementDes;
	}

	public DossierProcess setMovementDes(String movementDes) {
		this.movementDes = movementDes;
		return this;
	}
	
}
