package com.cesce.siniestros.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "NonPayments", description = "NonPayments")
public class NonPayments {

	@ApiModelProperty(position = 1, value = "Afp number", example="01120170", required = true)
	private BigDecimal afpNumber;
	
	@ApiModelProperty(position = 2, value = "Invoice number",example="4A4578999CVF", required = true)
	private String invNumber;
	 
	@ApiModelProperty(position = 3, value = "Invoice date",example="03072017", required = true)
	private BigDecimal invDate;
    
	@ApiModelProperty(position = 4, value = "Date of non-payment",example="03062017", required = true)
	private BigDecimal nonPayDate;
	
	@ApiModelProperty(position = 5, value = "Initial expiration date", example="03062017",required = true)
	private BigDecimal iniExpDate;
	
	@ApiModelProperty(position = 6, value = "Expiration date extended", example="03092017", required = true)
	private BigDecimal expDateExtended;
	
	@ApiModelProperty(position = 7, value = "Non-payment currency code (ISO format)",example="978", required = true)
	private BigDecimal nonCurrencyCode;
	
	@ApiModelProperty(position = 8, value = "Description of non-payment currency",example="Euro", required = true)
	private String desNonPayCurrency;
	
	@ApiModelProperty(position = 9, value = "Non-payment amount", example="450,43",required = true)
	private BigDecimal nonPayAmount;
	
	@ApiModelProperty(position = 10, value = "Non-payment reason code", example="010",required = false)
	private String nonPayReasonCode;
	
	@ApiModelProperty(position = 11, value = "Non-payment reason code", example="Not agree with a charge",required = false)
	private String nonPayReasonDesc;
	
	@ApiModelProperty(position = 12, value = "Non-payment's amount", required = false) 
	private NonPaymentsAmounts nonPaymentsAmounts;


	@ApiModelProperty(position = 13, value = "Classification text",example="Text standard", required = false)
	private String classText;

	@ApiModelProperty(position = 14, value = "Next settlement date",example="03092017", required = false)
	private BigDecimal nexSettDate;
	 
	@ApiModelProperty(position = 15, value = "Next payment deadline",example="03102017", required = false)
	private BigDecimal nextPayDeadline;
	
	@ApiModelProperty(position = 15, value = "Commitment payment index",example="S", required = false)
	private String comPayIndex;
	
	@ApiModelProperty(position = 16, value = "Certificate type",example="1", required = false)
	private String certType;
	
	@ApiModelProperty(position = 17, value = "Funding indicator",example="S", required = false)
	private String fundIndicator;
	
	@ApiModelProperty(position = 18, value = "Transferor name",example="Michael Ramson", required = false)
	private String transName;
	
	@ApiModelProperty(position = 18, value = "claim estimating coefficient",example="18,09", required = false)
	private BigDecimal claimEstimaCoefficient;
	///////////////////////////

	public BigDecimal getAfpNumber() {
		return afpNumber;
	}

	public NonPayments setAfpNumber(BigDecimal afpNumber) {
		this.afpNumber = afpNumber;
		return this;
	}

	public String getInvNumber() {
		return invNumber;
	}

	public NonPayments setInvNumber(String invNumber) {
		this.invNumber = invNumber;
		return this;
	}

	public BigDecimal getInvDate() {
		return invDate;
	}

	public NonPayments setInvDate(BigDecimal invDate) {
		this.invDate = invDate;
		return this;
	}

	public BigDecimal getNonPayDate() {
		return nonPayDate;
	}

	public NonPayments setNonPayDate(BigDecimal nonPayDate) {
		this.nonPayDate = nonPayDate;
		return this;
	}

	public BigDecimal getIniExpDate() {
		return iniExpDate;
	}

	public NonPayments setIniExpDate(BigDecimal iniExpDate) {
		this.iniExpDate = iniExpDate;
		return this;
	}

	public BigDecimal getExpDateExtended() {
		return expDateExtended;
	}

	public NonPayments setExpDateExtended(BigDecimal expDateExtended) {
		this.expDateExtended = expDateExtended;
		return this;
	}

	public BigDecimal getNonCurrencyCode() {
		return nonCurrencyCode;
	}

	public NonPayments setNonCurrencyCode(BigDecimal nonCurrencyCode) {
		this.nonCurrencyCode = nonCurrencyCode;
		return this;
	}

	public String getDesNonPayCurrency() {
		return desNonPayCurrency;
	}

	public NonPayments setDesNonPayCurrency(String desNonPayCurrency) {
		this.desNonPayCurrency = desNonPayCurrency;
		return this;
	}

	public BigDecimal getNonPayAmount() {
		return nonPayAmount;
	}

	public NonPayments setNonPayAmount(BigDecimal nonPayAmount) {
		this.nonPayAmount = nonPayAmount;
		return this;
	}

	public String getNonPayReasonCode() {
		return nonPayReasonCode;
	}

	public NonPayments setNonPayReasonCode(String nonPayReasonCode) {
		this.nonPayReasonCode = nonPayReasonCode;
		return this;
	}

	public String getNonPayReasonDesc() {
		return nonPayReasonDesc;
	}

	public NonPayments setNonPayReasonDesc(String nonPayReasonDesc) {
		this.nonPayReasonDesc = nonPayReasonDesc;
		return this;
	}

	public NonPaymentsAmounts getNonPaymentsAmounts() {
		return nonPaymentsAmounts;
	}

	public NonPayments setNonPaymentsAmounts(NonPaymentsAmounts nonPaymentsAmounts) {
		this.nonPaymentsAmounts = nonPaymentsAmounts;
		return this;
	}

	public String getClassText() {
		return classText;
	}

	public NonPayments setClassText(String classText) {
		this.classText = classText;
		return this;
	}

	public BigDecimal getNexSettDate() {
		return nexSettDate;
	}

	public NonPayments setNexSettDate(BigDecimal nexSettDate) {
		this.nexSettDate = nexSettDate;
		return this;
	}

	public BigDecimal getNextPayDeadline() {
		return nextPayDeadline;
	}

	public NonPayments setNextPayDeadline(BigDecimal nextPayDeadline) {
		this.nextPayDeadline = nextPayDeadline;
		return this;
	}

	public String getComPayIndex() {
		return comPayIndex;
	}

	public NonPayments setComPayIndex(String comPayIndex) {
		this.comPayIndex = comPayIndex;
		return this;
	}

	public String getCertType() {
		return certType;
	}

	public NonPayments setCertType(String certType) {
		this.certType = certType;
		return this;
	}

	public String getFundIndicator() {
		return fundIndicator;
	}

	public NonPayments setFundIndicator(String fundIndicator) {
		this.fundIndicator = fundIndicator;
		return this;
	}

	public String getTransName() {
		return transName;
	}

	public NonPayments setTransName(String transName) {
		this.transName = transName;
		return this;
	}

	public BigDecimal getClaimEstimaCoefficient() {
		return claimEstimaCoefficient;
	}

	public NonPayments setClaimEstimaCoefficient(BigDecimal claimEstimaCoefficient) {
		this.claimEstimaCoefficient = claimEstimaCoefficient;
		return this;
	}
	
	
}
