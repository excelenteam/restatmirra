package com.cesce.siniestros.entity;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author agustin.rodriguez
 * @version 1.0 18 de Julio de 2017
 *
 * Liquidation data
 */
@ApiModel(value = "DossierDoc", description = "")
public class DossierDoc {
	
	@ApiModelProperty(position = 7, value = "Document number", example = "123456", required = true)
	private BigDecimal documentNum;
	
	@ApiModelProperty(position = 8, value = "Document type description", example = "123ABC..50", required = true)
	private String documentTypeDes;

	@ApiModelProperty(position = 9, value = "Invoice number", example = "123ABC..100", required = true)
	private String invNumber;

	@ApiModelProperty(position = 10, value = "Status code of document", example = "123", required = true)
	private BigDecimal statusCode;
	
	@ApiModelProperty(position = 11, value = "Status description of document", example = "123ABC..50", required = true)
	private String statusDesc;

	@ApiModelProperty(position = 12, value = "Nature code of document: Original or copy", example = "12345678", required = true)
	private BigDecimal natureCode;
	
	@ApiModelProperty(position = 13, value = "Nature description of document: Original or copy", example = "123ABC..50", required = true)
	private String natureDesc;

	@ApiModelProperty(position = 14, value = "Request date", example = "12345678", required = false)
	private BigDecimal requestDate;
	
	@ApiModelProperty(position = 15, value = "Maximum delivery date", example = "12345678", required = false)
	private BigDecimal maxDeliveryDate;
	
	@ApiModelProperty(position = 16, value = "Claim last date", example = "12345678", required = false)
	private BigDecimal claimLastDate;
	
	@ApiModelProperty(position = 17, value = "Claim next date", example = "12345678", required = false)
	private BigDecimal claimNextDate;
	
	@ApiModelProperty(position = 18, value = "Update last date", example = "12345678", required = false)
	private BigDecimal updateLastDate;

	public BigDecimal getDocumentNum() {
		return documentNum;
	}

	public DossierDoc setDocumentNum(BigDecimal documentNum) {
		this.documentNum = documentNum;
		return this;
	}

	public String getDocumentTypeDes() {
		return documentTypeDes;
	}

	public DossierDoc setDocumentTypeDes(String documentTypeDes) {
		this.documentTypeDes = documentTypeDes;
		return this;
	}

	public String getInvNumber() {
		return invNumber;
	}

	public DossierDoc setInvNumber(String invNumber) {
		this.invNumber = invNumber;
		return this;
	}

	public BigDecimal getStatusCode() {
		return statusCode;
	}

	public DossierDoc setStatusCode(BigDecimal statusCode) {
		this.statusCode = statusCode;
		return this;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public DossierDoc setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
		return this;
	}

	public BigDecimal getNatureCode() {
		return natureCode;
	}

	public DossierDoc setNatureCode(BigDecimal natureCode) {
		this.natureCode = natureCode;
		return this;
	}

	public String getNatureDesc() {
		return natureDesc;
	}

	public DossierDoc setNatureDesc(String natureDesc) {
		this.natureDesc = natureDesc;
		return this;
	}

	public BigDecimal getRequestDate() {
		return requestDate;
	}

	public DossierDoc setRequestDate(BigDecimal requestDate) {
		this.requestDate = requestDate;
		return this;
	}

	public BigDecimal getMaxDeliveryDate() {
		return maxDeliveryDate;
	}

	public DossierDoc setMaxDeliveryDate(BigDecimal maxDeliveryDate) {
		this.maxDeliveryDate = maxDeliveryDate;
		return this;
	}

	public BigDecimal getClaimLastDate() {
		return claimLastDate;
	}

	public DossierDoc setClaimLastDate(BigDecimal claimLastDate) {
		this.claimLastDate = claimLastDate;
		return this;
	}

	public BigDecimal getClaimNextDate() {
		return claimNextDate;
	}

	public DossierDoc setClaimNextDate(BigDecimal claimNextDate) {
		this.claimNextDate = claimNextDate;
		return this;
	}

	public BigDecimal getUpdateLastDate() {
		return updateLastDate;
	}

	public DossierDoc setUpdateLastDate(BigDecimal updateLastDate) {
		this.updateLastDate = updateLastDate;
		return this;
	}
}
