package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Dossier", description = "Dossier entity")
public class Dossier {
	
	@ApiModelProperty(position = 1, value = "Policy mode", example = "677890200", required = true)
	private BigDecimal contractNumber;
	
	@ApiModelProperty(position = 2, value = "File number", example = "66781111", required = true)
	private BigDecimal fileNumber;
	
	@ApiModelProperty(position = 3, value = "Supplement number", example = "555534", required = true)
	private BigDecimal suppNumber;
	
	
	@ApiModelProperty(position = 4, value = "Information relative to debtor entity", required = true)
	private Debtor debtor;
	
	@ApiModelProperty(position = 5, value = "Description of the modality", example = "basic modality", required = true)
	private String descModality;
	
	@ApiModelProperty(position = 6, value = "Insured code", example = "600012345", required = true)
	private BigDecimal insCode;
	
	@ApiModelProperty(position = 7, value = "Country code of the policyholder", example = "180", required = true)
	private BigDecimal couCodePolicyHolder;
	
	@ApiModelProperty(position = 8, value = "Description of the policyholder's country", example = "European country", required = true)
	private String descCounCodePolicyHolder;
	
	@ApiModelProperty(position = 9, value = "Company code", example = "B", required = true)
	private BigDecimal compCode;
	
	@ApiModelProperty(position = 10, value = "Company description", example = "IT company", required = true)
	private String descCompany;
	
	@ApiModelProperty(position = 11, value = "Information relative to management entity", required = true)
	private Management management;
	
	
	//cod divisa expediente
	@ApiModelProperty(position = 12, value = "File currency code(ISO format)", example = "XYC", required = true)
	private String fileCurrCode;
	
	@ApiModelProperty(position = 12, value = "Description of the file currency", example = "ABV", required = true)
	private String descFileCurrency;
	
	@ApiModelProperty(position = 13, value = "Information relative to amounts entity",  required = false)
	private Amount amounts;

	public BigDecimal getContractNumber() {
		return contractNumber;
	}

	public Dossier setContractNumber(BigDecimal contractNumber) {
		this.contractNumber = contractNumber;
		return this;
	}

	public BigDecimal getFileNumber() {
		return fileNumber;
	}

	public Dossier setFileNumber(BigDecimal fileNumber) {
		this.fileNumber = fileNumber;
		return this;
	}

	public BigDecimal getSuppNumber() {
		return suppNumber;
	}

	public Dossier setSuppNumber(BigDecimal suppNumber) {
		this.suppNumber = suppNumber;
		return this;
	}

	public Debtor getDebtor() {
		return debtor;
	}

	public Dossier setDebtor(Debtor debtor) {
		this.debtor = debtor;
		return this;
	}

	public String getDescModality() {
		return descModality;
	}

	public Dossier setDescModality(String descModality) {
		this.descModality = descModality;
		return this;
	}

	public BigDecimal getInsCode() {
		return insCode;
	}

	public Dossier setInsCode(BigDecimal insCode) {
		this.insCode = insCode;
		return this;
	}

	public BigDecimal getCouCodePolicyHolder() {
		return couCodePolicyHolder;
	}

	public Dossier setCouCodePolicyHolder(BigDecimal couCodePolicyHolder) {
		this.couCodePolicyHolder = couCodePolicyHolder;
		return this;
	}

	public String getDescCounCodePolicyHolder() {
		return descCounCodePolicyHolder;
	}

	public Dossier setDescCounCodePolicyHolder(String descCounCodePolicyHolder) {
		this.descCounCodePolicyHolder = descCounCodePolicyHolder;
		return this;
	}

	public BigDecimal getCompCode() {
		return compCode;
	}

	public Dossier setCompCode(BigDecimal compCode) {
		this.compCode = compCode;
		return this;
	}

	public String getDescCompany() {
		return descCompany;
	}

	public Dossier setDescCompany(String descCompany) {
		this.descCompany = descCompany;
		return this;
	}

	public Management getManagement() {
		return management;
	}

	public Dossier setManagement(Management management) {
		this.management = management;
		return this;
	}

	public String getFileCurrCode() {
		return fileCurrCode;
	}

	public Dossier setFileCurrCode(String fileCurrCode) {
		this.fileCurrCode = fileCurrCode;
		return this;
	}

	public String getDescFileCurrency() {
		return descFileCurrency;
	}

	public Dossier setDescFileCurrency(String descFileCurrency) {
		this.descFileCurrency = descFileCurrency;
		return this;
	}

	public Amount getAmounts() {
		return amounts;
	}

	public Dossier setAmounts(Amount amounts) {
		this.amounts = amounts;
		return this;
	}
	
	
}
