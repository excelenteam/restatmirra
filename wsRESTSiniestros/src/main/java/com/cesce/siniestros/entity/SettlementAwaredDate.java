package com.cesce.siniestros.entity;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Liquidation pending date", description = "Complete data of a liquidation date that is pending")
public class SettlementAwaredDate {
	
	@ApiModelProperty(position = 1, value = "Payment limit date, format MMYYYY", example = "123456", required = false)
	private BigDecimal paymentLimitDate;
	
	@ApiModelProperty(position = 2, value = "Amount to compensate in payment limit date", example = "15.3", required = false)
	private BigDecimal impIndemnizacion;
	
	@ApiModelProperty(position = 3, value = "Currency in ISO code", example = "12A", required = true)
	private String currencyCode;
	
	@ApiModelProperty(position = 4, value = "Currency description", example = "123ABC..50", required = true)
	private String currencyDesc;

	public BigDecimal getPaymentLimitDate() {
		return paymentLimitDate;
	}

	public SettlementAwaredDate setPaymentLimitDate(BigDecimal paymentLimitDate) {
		this.paymentLimitDate = paymentLimitDate;
		return this;
	}

	public BigDecimal getImpIndemnizacion() {
		return impIndemnizacion;
	}

	public SettlementAwaredDate setImpIndemnizacion(BigDecimal impIndemnizacion) {
		this.impIndemnizacion = impIndemnizacion;
		return this;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public SettlementAwaredDate setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
		return this;
	}

	public String getCurrencyDesc() {
		return currencyDesc;
	}

	public SettlementAwaredDate setCurrencyDesc(String currencyDesc) {
		this.currencyDesc = currencyDesc;
		return this;
	}
	
}
