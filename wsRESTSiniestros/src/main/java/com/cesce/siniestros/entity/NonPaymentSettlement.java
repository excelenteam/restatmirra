package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Non payment of a liquidation", description = "Complete data of a non payment liquidation")
public class NonPaymentSettlement {

	@ApiModelProperty(position = 1, value = "Intern number", example = "1234567890", required = true)
	private BigDecimal numIntern;

	@ApiModelProperty(position = 2, value = "Payment date", example = "12345678", required = true)
	private BigDecimal datePayment;

	@ApiModelProperty(position = 3, value = "Liquidation porcentage", example = "123.45", required = true)
	private BigDecimal perLiquidation;

	@ApiModelProperty(position = 4, value = "Late interest porcentage", example = "123.45", required = false)
	private BigDecimal perLateInt;

	@ApiModelProperty(position = 5, value = "Days number of late interest", example = "123456", required = false)
	private BigDecimal daysLateInt;

	@ApiModelProperty(position = 6, value = "Currency of amount to compensate in ISO code", example = "12A", required = true)
	private String liqCurrCode;

	@ApiModelProperty(position = 7, value = "Currency of amount to compensate description", example = "123ABC..50", required = true)
	private String desLiqCurrency;

	@ApiModelProperty(position = 8, value = "Currency of non payment in ISO code", example = "12A", required = true)
	private String nonCurrCode;

	@ApiModelProperty(position = 9, value = "Currency of non payment description", example = "123ABC..50", required = true)
	private String desNonPayCurrency;

	@ApiModelProperty(position = 10, value = "Exchange rate of liquidation", example = "123456.1234567890", required = true)
	private BigDecimal exRateLiq;

	@ApiModelProperty(position = 11, value = "Amount to compensate of afp in this liquidation. Amount in non payment currency", example = "15.123", required = true)
	private BigDecimal amoCompCurr;

	@ApiModelProperty(position = 12, value = "Amount to compensate of afp in other liquidation. Amount in non payment currency", example = "15.123", required = false)
	private BigDecimal amoOtherCurr;

	@ApiModelProperty(position = 13, value = "Amount pending of afp in this liquidation. Amount in non payment currency", example = "15.123", required = false)
	private BigDecimal amoAwaredCurr;

	@ApiModelProperty(position = 14, value = "Amount to compensate of afp in this liquidation. Amount in liquidation currency", example = "15.123", required = true)
	private BigDecimal amoCompCurrLiq;

	@ApiModelProperty(position = 15, value = "Amount to compensate of afp in other liquidation. Amount in liquidation currency", example = "15.123", required = false)
	private BigDecimal amoOtherCurrLiq;

	@ApiModelProperty(position = 16, value = "Amount pending of afp in this liquidation. Amount in liquidation currency", example = "15.123", required = false)
	private BigDecimal amoAwaredCurrLiq;

	@ApiModelProperty(position = 17, value = "Amount of franchise pending to be consumed", example = "15.123", required = false)
	private BigDecimal amoAwaredFran;

	@ApiModelProperty(position = 18, value = "Amount of franchise consumed", example = "15.123", required = false)
	private BigDecimal amoConsumedFran;

	public BigDecimal getNumIntern() {
		return numIntern;
	}

	public NonPaymentSettlement setNumIntern(BigDecimal numIntern) {
		this.numIntern = numIntern;
		return this;
	}

	public BigDecimal getDatePayment() {
		return datePayment;
	}

	public NonPaymentSettlement setDatePayment(BigDecimal datePayment) {
		this.datePayment = datePayment;
		return this;
	}

	public BigDecimal getPerLiquidation() {
		return perLiquidation;
	}

	public NonPaymentSettlement setPerLiquidation(BigDecimal perLiquidation) {
		this.perLiquidation = perLiquidation;
		return this;
	}

	public BigDecimal getPerLateInt() {
		return perLateInt;
	}

	public NonPaymentSettlement setPerLateInt(BigDecimal perLateInt) {
		this.perLateInt = perLateInt;
		return this;
	}

	public BigDecimal getDaysLateInt() {
		return daysLateInt;
	}

	public NonPaymentSettlement setDaysLateInt(BigDecimal daysLateInt) {
		this.daysLateInt = daysLateInt;
		return this;
	}

	public String getLiqCurrCode() {
		return liqCurrCode;
	}

	public NonPaymentSettlement setLiqCurrCode(String liqCurrCode) {
		this.liqCurrCode = liqCurrCode;
		return this;
	}

	public String getDesLiqCurrency() {
		return desLiqCurrency;
	}

	public NonPaymentSettlement setDesLiqCurrency(String desLiqCurrency) {
		this.desLiqCurrency = desLiqCurrency;
		return this;
	}

	public String getNonCurrCode() {
		return nonCurrCode;
	}

	public NonPaymentSettlement setNonCurrCode(String nonCurrCode) {
		this.nonCurrCode = nonCurrCode;
		return this;
	}

	public String getDesNonPayCurrency() {
		return desNonPayCurrency;
	}

	public NonPaymentSettlement setDesNonPayCurrency(String desNonPayCurrency) {
		this.desNonPayCurrency = desNonPayCurrency;
		return this;
	}

	public BigDecimal getExRateLiq() {
		return exRateLiq;
	}

	public NonPaymentSettlement setExRateLiq(BigDecimal exRateLiq) {
		this.exRateLiq = exRateLiq;
		return this;
	}

	public BigDecimal getAmoCompCurr() {
		return amoCompCurr;
	}

	public NonPaymentSettlement setAmoCompCurr(BigDecimal amoCompCurr) {
		this.amoCompCurr = amoCompCurr;
		return this;
	}

	public BigDecimal getAmoOtherCurr() {
		return amoOtherCurr;
	}

	public NonPaymentSettlement setAmoOtherCurr(BigDecimal amoOtherCurr) {
		this.amoOtherCurr = amoOtherCurr;
		return this;
	}

	public BigDecimal getAmoAwaredCurr() {
		return amoAwaredCurr;
	}

	public NonPaymentSettlement setAmoAwaredCurr(BigDecimal amoAwaredCurr) {
		this.amoAwaredCurr = amoAwaredCurr;
		return this;
	}

	public BigDecimal getAmoCompCurrLiq() {
		return amoCompCurrLiq;
	}

	public NonPaymentSettlement setAmoCompCurrLiq(BigDecimal amoCompCurrLiq) {
		this.amoCompCurrLiq = amoCompCurrLiq;
		return this;
	}

	public BigDecimal getAmoOtherCurrLiq() {
		return amoOtherCurrLiq;
	}

	public NonPaymentSettlement setAmoOtherCurrLiq(BigDecimal amoOtherCurrLiq) {
		this.amoOtherCurrLiq = amoOtherCurrLiq;
		return this;
	}

	public BigDecimal getAmoAwaredCurrLiq() {
		return amoAwaredCurrLiq;
	}

	public NonPaymentSettlement setAmoAwaredCurrLiq(BigDecimal amoAwaredCurrLiq) {
		this.amoAwaredCurrLiq = amoAwaredCurrLiq;
		return this;
	}

	public BigDecimal getAmoAwaredFran() {
		return amoAwaredFran;
	}

	public NonPaymentSettlement setAmoAwaredFran(BigDecimal amoAwaredFran) {
		this.amoAwaredFran = amoAwaredFran;
		return this;
	}

	public BigDecimal getAmoConsumedFran() {
		return amoConsumedFran;
	}

	public NonPaymentSettlement setAmoConsumedFran(BigDecimal amoConsumedFran) {
		this.amoConsumedFran = amoConsumedFran;
		return this;
	}
	
}
