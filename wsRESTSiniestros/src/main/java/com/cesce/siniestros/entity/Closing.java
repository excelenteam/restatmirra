package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Closing", description = "Closing entity")
public class Closing {
	
	@ApiModelProperty(value = "Close date", example = "01012014")
	private BigDecimal closeDate;
	
	@ApiModelProperty(value = "Close type", example = "A")
	private String closeType;
	
	
	@ApiModelProperty(value = "Close reason code", example = "111")
	private BigDecimal closReasonCode;
	
	@ApiModelProperty(value = "Close reason description", example = "non payment")
	private BigDecimal closReasonDesc;

	public BigDecimal getCloseDate() {
		return closeDate;
	}

	public Closing setCloseDate(BigDecimal closeDate) {
		this.closeDate = closeDate;
		return this;
	}

	public String getCloseType() {
		return closeType;
	}

	public Closing setCloseType(String closeType) {
		this.closeType = closeType;
		return this;
	}

	public BigDecimal getClosReasonCode() {
		return closReasonCode;
	}

	public Closing setClosReasonCode(BigDecimal closReasonCode) {
		this.closReasonCode = closReasonCode;
		return this;
	}

	public BigDecimal getClosReasonDesc() {
		return closReasonDesc;
	}

	public Closing setClosReasonDesc(BigDecimal closReasonDesc) {
		this.closReasonDesc = closReasonDesc;
		return this;
	}
	
	
}
