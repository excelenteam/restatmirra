package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "NonPayment", description = "Information related to a non-payment")
public class NonPayment {
	
	@ApiModelProperty(position = 1, value = "Policy modality", required = true)
	private BigDecimal contNumber;
	
	@ApiModelProperty(position = 2, value = "File number", required = true)
	private BigDecimal fileNumber;
	
	@ApiModelProperty(position = 3, value = "Supplement number", required = true)
	private BigDecimal suppNumber;
	
	
	@ApiModelProperty(position = 4, value = "Information related to Debtor entity", required = true)
	private Debtor debtor;
	
	
	@ApiModelProperty(position = 5, value = "Insured's code", example = "600034589", required = true)
	private BigDecimal insuCode;
	
	@ApiModelProperty(position = 6, value = "Insured's name",example = "Michael Bristol", required = true)
	private String insuName;
	
	@ApiModelProperty(position = 7, value = "Invoice number", example = "34500077", required = false)
	private String invNumber;
	
	@ApiModelProperty(position = 8, value = "Invoice date", example = "25062017", required = false)
	private BigDecimal invDate;
	
	@ApiModelProperty(position = 9, value = "Information relative to the basic information about non-payment", required = false)
	private NonPaymentBasic nonPaymentBasic;
	
	
	@ApiModelProperty(position = 10, value = "Date of effect of the classiffication supplement",  example = "25072017", required = true)
	private BigDecimal dateEffClassSupplement;
	
	
	@ApiModelProperty(position = 11, value = "Information relative to the risk", required = true)
	private Risk risk;
	
	@ApiModelProperty(position = 12, value = "Coverage percentage", required = false)
	private BigDecimal coverPercentage;
	
	
	@ApiModelProperty(position = 13, value = "Exchange rate", required = false)
	private BigDecimal excRate;
	
	
	@ApiModelProperty(position = 14, value = "Classification code", required = false)
	private BigDecimal classCode;
	
	@ApiModelProperty(position = 15, value = "Classification description", required = false)
	private String classDesc;
	
	@ApiModelProperty(position = 16, value = "Classification text", required = false)
	private String classText;
	
	@ApiModelProperty(position = 17, value = "Amount of the risk limit to be consumed", required = false)
	private BigDecimal amoRiskLimConsumed;
	
	@ApiModelProperty(position = 18, value = "no indemnity code", required = false)
	private String noIndemityCode;
	
	
	@ApiModelProperty(position = 19, value = "Information relative to management currency", required = false)
	private Management  management;
	
	
	@ApiModelProperty(position = 20, value = "Information relative to annuity currency", required = false)
	private Annuity annuity;
	
	
	@ApiModelProperty(position = 18, value = "First motive out of coverage", required = false)
	private BigDecimal firstMotiveOutOfCoverage;
	
	@ApiModelProperty(position = 18, value = "Second motive out of coverage", required = false)
	private BigDecimal secondMotiveOutOfCoverage;

	public BigDecimal getContNumber() {
		return contNumber;
	}

	public NonPayment setContNumber(BigDecimal contNumber) {
		this.contNumber = contNumber;
		return this;
	}

	public BigDecimal getFileNumber() {
		return fileNumber;
	}

	public NonPayment setFileNumber(BigDecimal fileNumber) {
		this.fileNumber = fileNumber;
		return this;
	}

	public BigDecimal getSuppNumber() {
		return suppNumber;
	}

	public NonPayment setSuppNumber(BigDecimal suppNumber) {
		this.suppNumber = suppNumber;
		return this;
	}

	public Debtor getDebtor() {
		return debtor;
	}

	public NonPayment setDebtor(Debtor debtor) {
		this.debtor = debtor;
		return this;
	}

	public BigDecimal getInsuCode() {
		return insuCode;
	}

	public NonPayment setInsuCode(BigDecimal insuCode) {
		this.insuCode = insuCode;
		return this;
	}

	public String getInsuName() {
		return insuName;
	}

	public NonPayment setInsuName(String insuName) {
		this.insuName = insuName;
		return this;
	}

	public String getInvNumber() {
		return invNumber;
	}

	public NonPayment setInvNumber(String invNumber) {
		this.invNumber = invNumber;
		return this;
	}

	public BigDecimal getInvDate() {
		return invDate;
	}

	public NonPayment setInvDate(BigDecimal invDate) {
		this.invDate = invDate;
		return this;
	}

	public BigDecimal getDateEffClassSupplement() {
		return dateEffClassSupplement;
	}

	public NonPayment setDateEffClassSupplement(BigDecimal dateEffClassSupplement) {
		this.dateEffClassSupplement = dateEffClassSupplement;
		return this;
	}

	public Risk getRisk() {
		return risk;
	}

	public NonPayment setRisk(Risk risk) {
		this.risk = risk;
		return this;
	}

	public BigDecimal getCoverPercentage() {
		return coverPercentage;
	}

	public NonPayment setCoverPercentage(BigDecimal coverPercentage) {
		this.coverPercentage = coverPercentage;
		return this;
	}

	public BigDecimal getExcRate() {
		return excRate;
	}

	public NonPayment setExcRate(BigDecimal excRate) {
		this.excRate = excRate;
		return this;
	}

	public BigDecimal getClassCode() {
		return classCode;
	}

	public NonPayment setClassCode(BigDecimal classCode) {
		this.classCode = classCode;
		return this;
	}

	public String getClassDesc() {
		return classDesc;
	}

	public NonPayment setClassDesc(String classDesc) {
		this.classDesc = classDesc;
		return this;
	}

	public String getClassText() {
		return classText;
	}

	public NonPayment setClassText(String classText) {
		this.classText = classText;
		return this;
	}

	public BigDecimal getAmoRiskLimConsumed() {
		return amoRiskLimConsumed;
	}

	public NonPayment setAmoRiskLimConsumed(BigDecimal amoRiskLimConsumed) {
		this.amoRiskLimConsumed = amoRiskLimConsumed;
		return this;
	}

	public String getNoIndemityCode() {
		return noIndemityCode;
	}

	public NonPayment setNoIndemityCode(String noIndemityCode) {
		this.noIndemityCode = noIndemityCode;
		return this;
	}

	public Management getManagement() {
		return management;
	}

	public NonPayment setManagement(Management management) {
		this.management = management;
		return this;
	}

	public Annuity getAnnuity() {
		return annuity;
	}

	public NonPayment setAnnuity(Annuity annuity) {
		this.annuity = annuity;
		return this;
	}

	public BigDecimal getFirstMotiveOutOfCoverage() {
		return firstMotiveOutOfCoverage;
	}

	public NonPayment setFirstMotiveOutOfCoverage(BigDecimal firstMotiveOutOfCoverage) {
		this.firstMotiveOutOfCoverage = firstMotiveOutOfCoverage;
		return this;
	}

	public BigDecimal getSecondMotiveOutOfCoverage() {
		return secondMotiveOutOfCoverage;
	}

	public NonPayment setSecondMotiveOutOfCoverage(BigDecimal secondMotiveOutOfCoverage) {
		this.secondMotiveOutOfCoverage = secondMotiveOutOfCoverage;
		return this;
	}

	public NonPaymentBasic getNonPaymentBasic() {
		return nonPaymentBasic;
	}

	public NonPayment setNonPaymentBasic(NonPaymentBasic nonPaymentBasic) {
		this.nonPaymentBasic = nonPaymentBasic;
		return this;
	}
	
	
}
