package com.cesce.siniestros.entity;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "NonPaymentsAmounts", description = "Ratio of amounts related to non-payment")
public class NonPaymentsAmounts {
	
	@ApiModelProperty(value = "Covered amount", example = "1.795,04")
	private BigDecimal covAmount;
	
	@ApiModelProperty(value = "Amount compensable", example = "650,05")
	private BigDecimal amoCompensable;

	@ApiModelProperty(value = "Amount to be awared", example = "150,05")
	private BigDecimal amoAwared;
	
	@ApiModelProperty(value = "Amount charged", example = "375,00")
	private BigDecimal amoCharged;
	
	@ApiModelProperty(value = "Amount charged", example = "1375,56")
	private BigDecimal amoRecovered;
	
	@ApiModelProperty(value = "Amount of expenses", example = "3375,00")
	private BigDecimal amoOfExpenses;

	public BigDecimal getCovAmount() {
		return covAmount;
	}

	public NonPaymentsAmounts setCovAmount(BigDecimal covAmount) {
		this.covAmount = covAmount;
		return this;
	}

	public BigDecimal getAmoCompensable() {
		return amoCompensable;
	}

	public NonPaymentsAmounts setAmoCompensable(BigDecimal amoCompensable) {
		this.amoCompensable = amoCompensable;
		return this;
	}

	public BigDecimal getAmoAwared() {
		return amoAwared;
	}

	public NonPaymentsAmounts setAmoAwared(BigDecimal amoAwared) {
		this.amoAwared = amoAwared;
		return this;
	}

	public BigDecimal getAmoCharged() {
		return amoCharged;
	}

	public NonPaymentsAmounts setAmoCharged(BigDecimal amoCharged) {
		this.amoCharged = amoCharged;
		return this;
	}

	public BigDecimal getAmoRecovered() {
		return amoRecovered;
	}

	public NonPaymentsAmounts setAmoRecovered(BigDecimal amoRecovered) {
		this.amoRecovered = amoRecovered;
		return this;
	}

	public BigDecimal getAmoOfExpenses() {
		return amoOfExpenses;
	}

	public NonPaymentsAmounts setAmoOfExpenses(BigDecimal amoOfExpenses) {
		this.amoOfExpenses = amoOfExpenses;
		return this;
	}
	
	
	
	
}
