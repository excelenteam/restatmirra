package com.cesce.siniestros.entity;

import io.swagger.annotations.ApiParam;

public class CustomerQueryParams {
	
	@ApiParam(value = "Query source identifier", example = "12345ABCDE", required=false)
	public String originId;
	
	@ApiParam(value = "Code of customer's user", example = "1234567890ABCDEFGHIJKLMNO", required=false)
	public String customerUserCode;

	@ApiParam(value = "Number of policy", example = "12345678901", required=false)
	public Integer contractNumber;

	public String getOriginId() {
		return originId;
	}

	public CustomerQueryParams withOriginId(String originId) {
		this.originId = originId;
		return this;
	}

	public String getCustomerUserCode() {
		return customerUserCode;
	}

	public CustomerQueryParams withCustomerUserCode(String customerUserCode) {
		this.customerUserCode = customerUserCode;
		return this;
	}

	public Integer getContractNumber() {
		return contractNumber;
	}

	public CustomerQueryParams withContractNum(Integer contractNumber) {
		this.contractNumber = contractNumber;
		return this;
	}
	
	

}