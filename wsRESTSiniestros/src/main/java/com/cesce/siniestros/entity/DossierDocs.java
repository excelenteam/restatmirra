package com.cesce.siniestros.entity;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @author agustin.rodriguez
 * @version 1.0 18 de Julio de 2017
 *
 * Liquidation data
 */
@ApiModel(value = "DossierDocs", description = "")
public class DossierDocs {
	
	@ApiModelProperty(position = 1, value = "Dossier number", example = "12345678", required = true)
	private BigDecimal doosierNum;

	@ApiModelProperty(position = 2, value = "Contract number", example = "12345678901", required=true)
	public Integer contractNum;
	
	@ApiModelProperty(position = 3, value = "Debtor code", example = "123456789", required = true)
	private BigDecimal debtorCode;

	@ApiModelProperty(position = 4, value = "Debtor name", example = "123ABC..100", required = true)
	private String debtorName;

	@ApiModelProperty(position = 5, value = "Insured code", example = "123456789", required = true)
	private BigDecimal insuredCode;
	
	@ApiModelProperty(position = 6, value = "Insured name", example = "123ABC..100", required = true)
	private String insuredName;

	//@ApiModelProperty(position = 7, value = "List of documents", required = true)
	private List<DossierDoc> docList;

	@ApiModelProperty(position = 19, value = "Returned ocurrence number", example = "12345", required = true)
	private BigDecimal returnOcurrenceNum;
	
	@ApiModelProperty(position = 20, value = "Page indicator. First page (P) Last page (U)", example = "P", required = true)
	private char pageInd;
	
	@ApiModelProperty(position = 21, value = "Output replacement value", example = "12345..23", required = true)
	private BigDecimal outputReplacementValue;

	public BigDecimal getDoosierNum() {
		return doosierNum;
	}

	public DossierDocs setDoosierNum(BigDecimal doosierNum) {
		this.doosierNum = doosierNum;
		return this;
	}

	public Integer getContractNum() {
		return contractNum;
	}

	public DossierDocs setContractNum(Integer contractNum) {
		this.contractNum = contractNum;
		return this;
	}

	public BigDecimal getDebtorCode() {
		return debtorCode;
	}

	public DossierDocs setDebtorCode(BigDecimal debtorCode) {
		this.debtorCode = debtorCode;
		return this;
	}

	public String getDebtorName() {
		return debtorName;
	}

	public DossierDocs setDebtorName(String debtorName) {
		this.debtorName = debtorName;
		return this;
	}

	public BigDecimal getInsuredCode() {
		return insuredCode;
	}

	public DossierDocs setInsuredCode(BigDecimal insuredCode) {
		this.insuredCode = insuredCode;
		return this;
	}

	public String getInsuredName() {
		return insuredName;
	}

	public DossierDocs setInsuredName(String insuredName) {
		this.insuredName = insuredName;
		return this;
	}

	public List<DossierDoc> getDocList() {
		return docList;
	}

	public DossierDocs setDocList(List<DossierDoc> docList) {
		this.docList = docList;
		return this;
	}

	public BigDecimal getReturnOcurrenceNum() {
		return returnOcurrenceNum;
	}

	public DossierDocs setReturnOcurrenceNum(BigDecimal returnOcurrenceNum) {
		this.returnOcurrenceNum = returnOcurrenceNum;
		return this;
	}

	public char getPageInd() {
		return pageInd;
	}

	public DossierDocs setPageInd(char pageInd) {
		this.pageInd = pageInd;
		return this;
	}

	public BigDecimal getOutputReplacementValue() {
		return outputReplacementValue;
	}

	public DossierDocs setOutputReplacementValue(BigDecimal outputReplacementValue) {
		this.outputReplacementValue = outputReplacementValue;
		return this;
	}
	
}
