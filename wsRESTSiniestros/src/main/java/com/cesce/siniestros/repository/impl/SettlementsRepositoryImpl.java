package com.cesce.siniestros.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cesce.siniestros.entity.CustomerQueryParams;
import com.cesce.siniestros.entity.NonPaymentSettlement;
import com.cesce.siniestros.entity.Settlement;
import com.cesce.siniestros.entity.SettlementsQueryParams;
import com.cesce.siniestros.repository.SettlementsRepository;

/**
 * 
 * @author agustin.rodriguez
 *
 */
@Repository
public class SettlementsRepositoryImpl implements SettlementsRepository{

	@Override
	public List<Settlement> getList(Integer customerCode, String situationCode, String languageCode,
			CustomerQueryParams custParams, SettlementsQueryParams params) {
		return new ArrayList<Settlement>();
	}

	@Override
	public List<Settlement> getList(Integer customerCode, String languageCode, Double internalNumber,
			String customerUserCode, Double contractNumber) {
		return new ArrayList<Settlement>();
	}

	@Override
	public Settlement getSettlement(Integer customerCode, String languageCode, CustomerQueryParams custParams,
			Integer settlementNum) {
		return new Settlement();
	}

	
	@Override
	public List<NonPaymentSettlement> getNonPaymentSettlement(Integer customerCode, String languageCode,
			Integer settlementNum, CustomerQueryParams custParams) {
		return new ArrayList<NonPaymentSettlement>();
	}
	

	

	
	
}
