package com.cesce.siniestros.repository;

import java.util.List;

import com.cesce.siniestros.entity.NonPayment;
import com.cesce.siniestros.entity.NonPayments;

public interface NonPaymentRepository {

//    Info save(Info info);
//
//    Info findOne(Long id);

    List<NonPayments> findAll();

    
    // cambiar por NonPayment
    NonPayment findOne(Integer id);
//
//    void delete(Long id);
}
