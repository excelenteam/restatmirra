package com.cesce.siniestros.repository.impl;

import com.cesce.siniestros.entity.CustomerQueryParams;
import com.cesce.siniestros.entity.Dossier;
import com.cesce.siniestros.entity.DossierDocs;
import com.cesce.siniestros.entity.DossierProcess;
import com.cesce.siniestros.entity.NonPaymentSettlement;
import com.cesce.siniestros.entity.Recovers;
import com.cesce.siniestros.entity.SettlementAwaredDate;
import com.cesce.siniestros.repository.DossierRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class DossierRepositoryMock implements DossierRepository {

	@Override
	public Dossier getDossierByID(Integer customerCode, String languageCode, Integer dossierNumber) {
		// TODO Auto-generated method stub
		return new Dossier().setDescCompany("ABC");
	}

	@Override
	public Dossier getClassificationSupplements(Integer customerCode, String languageCode, Integer contractNumber,
			Integer suppNumber, Integer debtorCode, String originId, String customerUserCode, Integer dossierNumber) {
		// TODO Auto-generated method stub
		return new Dossier().setDescCompany("ABC");
	}

	@Override
	public List<Dossier> getListDossiers(Integer customerCode, String languageCode, String situationCode,
			CustomerQueryParams custParams, Integer dossierNum, Integer supplementNum, String debtorName,
			String debtorCountryCode, String debtorFiscalCode, String referenceCode) {
		return new ArrayList<Dossier>();
	}

	@Override
	public DossierDocs getDossierDocumentList(Integer customerCode, String languageCode, Integer ocurrenceNum,
			CustomerQueryParams custParams, Integer dossierNum, Integer supplementNum, Integer inputReplacementValue) {
		return new DossierDocs();
	}
	
	@Override
	public List<SettlementAwaredDate> getPendingDates(Integer customerCode, String languageCode, Integer dossierNum,
			CustomerQueryParams custParams) {
		return new ArrayList<SettlementAwaredDate>();
	}
	@Override
	public List<Recovers> getListRecovers(Integer customerCode, String languageCode, Integer dossierNum,
			CustomerQueryParams custParams) {
		return new ArrayList<Recovers>();
	}
	
	@Override
	public List<String> getDossiersManagerRecovery(Integer customerCode, String languageCode, String dossierNum,
			CustomerQueryParams custParams) {
		return new ArrayList<String>();
	}
	
	

	@Override
	public List<DossierProcess> getDossierManagerProcessing(Integer customerCode, String languageCode, String dossierNum,
			CustomerQueryParams custParams) {
		return new ArrayList<DossierProcess>();
	}

}
