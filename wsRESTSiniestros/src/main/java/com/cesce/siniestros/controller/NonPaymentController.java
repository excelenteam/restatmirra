package com.cesce.siniestros.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cesce.siniestros.entity.CustomerQueryParams;
import com.cesce.siniestros.entity.NonPayment;
import com.cesce.siniestros.entity.NonPayments;
import com.cesce.siniestros.exception.CesceInvalidParameterException;
import com.cesce.siniestros.service.NonPaymentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2

@RestController
@RequestMapping("/claims/v1")

@Api(value = "claims", produces = "application/json", tags = "Dossiers", description = "Dossiers")

public class NonPaymentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(NonPaymentController.class);

	@Autowired
	private NonPaymentService nonPaymentService;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "{customerCode}/dossiers/{languageCode}/nonpayments/{dossierNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get all non-payments of a dossier number", notes = "Returns all non-payments of a dossier number ")
	public @ResponseBody List<NonPayments> getListNonPayments(HttpServletResponse response, HttpServletRequest request,
			@PathVariable Integer customerCode , @PathVariable String languageCode, @PathVariable Integer dossierNumber,
			CustomerQueryParams customerQueryParams) {

		validateInputParameters(customerCode, languageCode, dossierNumber);
		LOGGER.info("GET -> /claims/{customerCode}", customerCode);
		LOGGER.info("GET -> /claims/{languageCode}", languageCode);
		LOGGER.info("GET -> /claims/{dossierNumber}", dossierNumber);
		return nonPaymentService.findAll();

	}

	

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "{customerCode}/dossiers/{languageCode}/{dossierNumber}/nonpayments/{interNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get a non-payment of a dossier number", notes = "Returns a non-payment of a dossier number ")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "No data found") })
	public @ResponseBody NonPayment getNonPaymentById(HttpServletResponse response, HttpServletRequest request,
			@PathVariable Integer customerCode, @PathVariable String languageCode, @PathVariable Integer dossierNumber,
			@PathVariable String interNumber, CustomerQueryParams customerQueryParams) {
		
		LOGGER.info("GET -> /claims/{customerCode}", customerCode);
		LOGGER.info("GET -> /claims/{languageCode}", languageCode);
		LOGGER.info("GET -> /claims/{fileNumber}", dossierNumber);
		
		validateInputParameters(customerCode, languageCode, dossierNumber);
		return nonPaymentService.findOne(dossierNumber);

	}
	
	private void validateInputParameters(Integer clientCode, String languageCode, Integer fileNumber) {
		if (!clientCode.toString().matches("\\d{9}")) {
			throw new CesceInvalidParameterException("Customer code must be a 9 digits number");
		}
		if (languageCode.length() < 3) {
			throw new CesceInvalidParameterException("language Code must be a 3 character string");
		}
		if (fileNumber.toString().matches("\\d8}")) {
			throw new CesceInvalidParameterException("FileNumber must be a 3 digits number");
		}
	}
}
