package com.cesce.siniestros.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cesce.siniestros.entity.CustomerQueryParams;
import com.cesce.siniestros.entity.Dossier;
import com.cesce.siniestros.entity.DossierDocs;
import com.cesce.siniestros.entity.DossierProcess;
import com.cesce.siniestros.entity.Recovers;
import com.cesce.siniestros.entity.SettlementAwaredDate;
import com.cesce.siniestros.service.DossierService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2

@RestController
@RequestMapping("/claims/v1")

@Api(value = "claims", produces = "application/json", tags = "Dossiers", description = "Dossiers")
public class DossierController {
	
	@Autowired
	private DossierService dossierService;

	//1ª combinación de datos obligatoria
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "{customerCode}/dossiers/{languageCode}/dossier/{dossierNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get a dossier by dossier number", notes = "Returns dossier by dossier number ")
//	public @ResponseBody Dossier getDossierByID(HttpServletResponse response, HttpServletRequest request,
//			@PathVariable Integer customerCode , @PathVariable String languageCode, @PathVariable Integer dossierNumber) {
	public @ResponseBody Dossier getDossierByID(HttpServletResponse response, HttpServletRequest request,
			@ApiParam(defaultValue="xyz", required = true, type="path") @PathVariable Integer customerCode , @PathVariable String languageCode, @PathVariable Integer dossierNumber) {
	return dossierService.getDossierByID(customerCode, languageCode, dossierNumber);
 
	}

	//2ª combinación de datos obligatoria
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "{customerCode}/dossiers/{languageCode}/{contractNumber}/anonymous/{suppNumber}/{debtorCode}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get a dossier by anonymous sorting supplements", notes = "Returns dossier by anonymous sorting supplements")
	public @ResponseBody Dossier getDossierByAnonymousSorting(HttpServletResponse response, HttpServletRequest request,
						 @PathVariable Integer customerCode, @PathVariable String languageCode, @PathVariable Integer contractNumber,
						 @PathVariable Integer suppNumber, @PathVariable Integer debtorCode,@RequestParam(required=false) String originId,
						 @RequestParam(required=false) String customerUserCode,@RequestParam(required=false) Integer dossierNumber ) {

		return dossierService.getClassificationSupplements(customerCode, languageCode, contractNumber, 
				suppNumber, debtorCode, originId, customerUserCode, dossierNumber);

	}
	
	//3ª  combinación de datos obligatoria
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "{customerCode}/dossiers/{languageCode}/{contractNumber}/anonymous/{suppNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get a dossier by anonymous sorting supplements", notes = "Returns dossier by anonymous sorting supplements")
	public @ResponseBody Dossier getDossierByClassificationSupplements(HttpServletResponse response, HttpServletRequest request,
						 @PathVariable Integer customerCode, @PathVariable String languageCode, @PathVariable Integer contractNumber,
						 @PathVariable Integer suppNumber,@RequestParam(required=false) String originId,
						 @RequestParam(required=false) String customerUserCode,@RequestParam(required=false) Integer dossierNumber ) {

		return dossierService.getClassificationSupplements(customerCode, languageCode, contractNumber, 
				suppNumber, null, originId, customerUserCode, dossierNumber);

	}
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/dossiers/{languageCode}/{situationCode}", method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get dossiers", notes = "Return a dossier list")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Dossier not found") 
	})
	public @ResponseBody List<Dossier> getDossiers(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable String situationCode,
													CustomerQueryParams custParams,
													@RequestParam( required = false ) Integer dossierNum,
													@RequestParam( required = false ) Integer supplementNum,
													@RequestParam( required = false ) String debtorName,
													@RequestParam( required = false ) String debtorCountryCode,
													@RequestParam( required = false ) String debtorFiscalCode,
													@RequestParam( required = false ) String referenceCode) {

		List<Dossier> listaDatos = dossierService.getListDossiers(customerCode, languageCode, situationCode, custParams, dossierNum,
				supplementNum, debtorName, debtorCountryCode, debtorFiscalCode, referenceCode);
		return listaDatos;
	}
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/dossiers/{languageCode}/documentList/{ocurrenceNum}", method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get document list of a dossier", notes = "Return a document list of a dossier")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Dossier not found") 
	})
	public @ResponseBody DossierDocs getDossierDocumentList(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable Integer ocurrenceNum,
													CustomerQueryParams custParams,
													@RequestParam( required = false ) Integer dossierNum,
													@RequestParam( required = false ) Integer supplementNum,
													@RequestParam( required = false ) Integer inputReplacementValue) {

		DossierDocs datos = dossierService.getDossierDocumentList(customerCode, languageCode, ocurrenceNum, custParams, dossierNum, supplementNum, inputReplacementValue);
		return datos;
	}
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/dossiers/{languageCode}/pendingDates/{dossierNum}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get pending dates of a settlement", notes = "Get pending dates of a settlement")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Settlement not found") 
	})
	public List<SettlementAwaredDate> getPendingDates(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable Integer dossierNum, 
													CustomerQueryParams custParams) {
		
		List<SettlementAwaredDate> pendDates = dossierService.getPendingDates(customerCode, languageCode, dossierNum, custParams);
		return pendDates;
	}
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/dossiers/{languageCode}/recoveries/{dossierNum}", method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get recoveries of a dossier", notes = "Return a recover list of a dossier")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Recover not found") 
	})
	public @ResponseBody List<Recovers> getRecovers(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable Integer dossierNum,
													CustomerQueryParams custParams) {

		List<Recovers> listaDatos = dossierService.getListRecovers(customerCode, languageCode, dossierNum, custParams);
		return listaDatos;
	}
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/dossiers/recoveryManager/{languageCode}/dossier/{dossierNum}", method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get list of recovery manager of a dossier", notes = "Return list of recovery manager of a dossier")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Dossier not found") 
	})
	public @ResponseBody List<String> getRecoveriesManagerDossier(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable String dossierNum,
													CustomerQueryParams custParams) {

		List<String> listaDatos = dossierService.getDossiersManagerRecovery(customerCode, languageCode, dossierNum, custParams);
		return listaDatos;
	}
	

	
	
	
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{customerCode}/dossiers/processingManager/{languageCode}/dossier/{dossierNum}", method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get list of a dossier in processing manager", notes = "Return a processing manager list of a dossier")
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message = "Dossier not found") 
	})
	public @ResponseBody List<DossierProcess> getProcessingManagerDossier(HttpServletResponse response, HttpServletRequest request,
													@PathVariable Integer customerCode,
													@PathVariable String languageCode,
													@PathVariable String dossierNum,
													CustomerQueryParams custParams) {

		List<DossierProcess> listaDatos = dossierService.getDossierManagerProcessing(customerCode, languageCode, dossierNum, custParams);
		return listaDatos;
	}
	
}
