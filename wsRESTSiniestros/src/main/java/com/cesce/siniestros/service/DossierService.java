package com.cesce.siniestros.service;

import java.util.List;

import com.cesce.siniestros.entity.CustomerQueryParams;
import com.cesce.siniestros.entity.Dossier;
import com.cesce.siniestros.entity.DossierDocs;
import com.cesce.siniestros.entity.DossierProcess;
import com.cesce.siniestros.entity.NonPaymentSettlement;
import com.cesce.siniestros.entity.Recovers;
import com.cesce.siniestros.entity.SettlementAwaredDate;

public interface DossierService {

	Dossier getDossierByID(Integer customerCode, String languageCode, Integer dossierNumber);

	Dossier getClassificationSupplements(Integer customerCode, String languageCode, Integer contractNumber,
			Integer suppNumber, Integer debtorCode, String originId, String customerUserCode, Integer dossierNumber);

	public List<Dossier> getListDossiers(Integer customerCode, String languageCode, String situationCode,
			CustomerQueryParams custParams, Integer dossierNum, Integer supplementNum, String debtorName,
			String debtorCountryCode, String debtorFiscalCode, String referenceCode);

	public DossierDocs getDossierDocumentList(Integer customerCode, String languageCode, Integer ocurrenceNum,
			CustomerQueryParams custParams, Integer dossierNum, Integer supplementNum, Integer inputReplacementValue);

	public List<SettlementAwaredDate> getPendingDates(Integer customerCode, String languageCode, Integer dossierNum,
			CustomerQueryParams custParams);

	public List<Recovers> getListRecovers(Integer customerCode, String languageCode, Integer dossierNum,
			CustomerQueryParams custParams);
	
	public List<String> getDossiersManagerRecovery(Integer customerCode, String languageCode, String dossierNum,
			CustomerQueryParams custParams);
	
	

	public List<DossierProcess> getDossierManagerProcessing(Integer customerCode, String languageCode, String dossierNum,
			CustomerQueryParams custParams);
}
