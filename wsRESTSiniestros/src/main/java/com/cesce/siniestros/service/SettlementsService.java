package com.cesce.siniestros.service;

import java.util.List;

import com.cesce.siniestros.entity.CustomerQueryParams;
import com.cesce.siniestros.entity.NonPaymentSettlement;
import com.cesce.siniestros.entity.Settlement;
import com.cesce.siniestros.entity.SettlementsQueryParams;

/**
 * 
 * @author agustin.rodriguez
 *
 */
public interface SettlementsService {

	public List<Settlement> getList(Integer customerCode, String situationCode, String languageCode,
			CustomerQueryParams custParams, SettlementsQueryParams params);

	public List<Settlement> getList(Integer customerCode, String languageCode, Double internalNumber,
			String customerUserCode, Double contractNumber);

	public Settlement getSettlement(Integer customerCode, String languageCode, CustomerQueryParams custParams,
			Integer settlementNum);

	public List<NonPaymentSettlement> getNonPaymentSettlement(Integer customerCode, String languageCode,
			Integer settlementNum, CustomerQueryParams custParams);

}
