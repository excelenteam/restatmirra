#/bin/bash
# $1 is the file name
# usage: this_script  <filename>
IP_address="192.168.183.12"
username="pi"
password=ph34V3r3
file="/home/altamira/git/restatmirra/PriceSpyServices/target/PriceSpyServices-1.0.war"
fileTo="/opt/des/tomcat/webapps"

#echo "
# verbose
# open $IP_address
# USER $username $password

# put $file
# bye
#" | ftp -n > ftp_$$.log

sshpass -p $password | scp -r $file $username@$IP_address:$fileTo

# Instalar: sshpass
# Ejecutar: sshpass -p 'password' 

#scp -r $file $username@$IP_address:$fileTo
